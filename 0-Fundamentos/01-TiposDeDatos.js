/*
Tipos de datos
*/
// number
let num = 25;
console.log(num);
num = 25.5;
console.log(num);

// string
let nombre = "Fabricio";
let apellido = "Ojeda";
let nombreCompleto = `Nombre completo: ${nombre} ${apellido}`;
console.log(nombre);
console.log(apellido);
console.log(nombreCompleto);

// boolean
let flag = false;
console.log(flag);
flag = true;
console.log(flag);

// undefined
let x;
console.log(x);
let indefinido = undefined;
console.log(indefinido);

// null
const nulo = null;
console.log(nulo);

/*
    Tipos de datos estructurales
*/

// object - {clave: valor, clave2: valor2}
let persona = {
  nombre: "Juan",
  apellido: "Perez",
  edad: 25,
  fechaNacimiento: null,
  esAlto: false,
  ciudadesVisitadas: ["Buenos Aires", "Chiloe", "Rio negro"],
  direccion: {
    calle: "Cod 2437",
    altura: 1783,
    depto: 18,
  },
};
console.log(persona);

/**
 * Tipos de datos de tipo lista
 */
// Array
const paises = ["México", "Argentina", "Chile"];
console.log(paises);
const varios = [21, true, { nombre: "Carlos", apellido: "Lara" }, [1, 2, 3]];
console.log(varios);
