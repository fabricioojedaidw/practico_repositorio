'use strict';

/**
 *  var vs let
 * 
 * var = variable global
 * 
 * let = varialbe local
 */

//bloque de codigo
{
    var nombreCompleto = 'Fabricio Ojeda';
    console.log(nombreCompleto);
}
console.log(nombreCompleto);

{
    let saludo = 'Hola soy Fabricio';
    console.log(saludo);
}
// console.log(saludo);

/**
 *Buenas prácticas con constantes
 * 
 *  Escribir el nombre con mayúsculas y separadas con _
 * 
 *  Debe estar inicializada
 * 
 */
const PI = 3.14;
console.log(PI);  

// template string
let nombre = 'Fabricio';
let apellido = 'Ojeda';
let saludo = `Hola soy ${nombre} ${apellido}
Como estas?`;
console.log(saludo);