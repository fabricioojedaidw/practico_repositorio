'use strict';

/**
 * Operadores
 */

/**
 * Operadores de asignacion
 */

// Operador de asignacion
var x = 2;
const y = 5;

// Asignacion de adicion
x += y;
console.log(x);

// Asignacion de resta
x -= y;
console.log(x);

// Asignacion de multiplicacion
x *= y;
console.log(x);

// Asignacion de division
x /= y;
console.log(x);

// Asignacion de potenciacion
x **= y;
console.log(x);

// Asignacion de modulo
x %= y;
console.log(x);

/**
 * Operadores de comparacion
 * 
 *  Siempre utilizar el estricto
 */

// Igual
console.log(3 == '3');
console.log(3 == 3);
//Igual estricto
console.log(3 === '3');
console.log(3 === 3);

// Distinto
console.log(3 != '3');
console.log(3 != 3);
//Distinto estricto
console.log(3 != '3');
console.log(3 !== 3);

// >, >=, <, <=
console.log( 3 > 4);
console.log( 3 >= 3);
console.log(2 < 4);
console.log(2 <= 2);

/**
 * Operadores aritmeticos
 * 
 * + - / * ** % 
 */

/**
 * Incremento y decremento
 * 
 * ++variable --variable
 */

let numero = 0;
//pre incremento
console.log(++numero);
//post incremento
console.log(numero++);
console.log(numero);

// pre decremento
console.log(--numero);
//post decremento
console.log(numero--);
console.log(numero);

/**
 * Operadores lógicos
 * 
 * && || 
 */

// AND &&
console.log(true && false);
console.log(true && true);
console.log(2<3 && 2<5);
console.log(2<3 && 2>5);


// OR ||
console.log(false || true);
console.log(false || false);
console.log(2<3 || 2<5);
console.log(2>3 || 2>5);

// NOT
console.log(!true);
console.log(!false);

/**
 * Otros operadores
 */

// Concatenacion de cadenas
var nombre = 'Fabricio';
var apellido = 'Ojeda';
var nombreCompleto = nombre + ' ' + apellido;
console.log(nombreCompleto);

// Conicional o ternario
console.log((2 < 3) ? 'Mayor' : 'Menor');

/**
 * Desestructuracion
 * 
 * Objetos:
 *      Desarma un objeto y obtiene una propiedad especifica de el
 * 
 * Arreglos
 */

//Desestructuracion de objetos
var persona = {
    fullName: 'Jaime Jara',
    age: 27
}
var {fullName, age: edad} = persona;
console.log(fullName);
console.log(edad); //se renombro la propiedad age: edad
console.log(persona);

//Desestructuracion de arreglos
var arreglo = [4, 2, 5, 1, 9];
var [pos1, , , pos4] = arreglo;
console.log(pos1);
console.log(pos4);

/**
 * Operador de miembro o acceso de propiedad
 */

// Notacion por punto
var aprendiendo = {
    nombre: 'Javascript',
    extencion: '.js'
}
console.log(aprendiendo.nombre);
console.log(aprendiendo.extencion);

// Notacion por corchetes
console.log(aprendiendo['nombre']);
console.log(aprendiendo['extencion']);

// Notacion por corchetes en arreglos
var edades = [21, 23, 33, 45, 66];
console.log(edades[0]);


//typeof - Operador de determinacion de tipo
console.log(typeof 'Fabricio');
console.log(typeof 21);
console.log(typeof persona);
console.log(typeof edades);

