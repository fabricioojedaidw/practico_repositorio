'use strict';

/**
 * Ciclos
 */

// While
var i = 0;
while (i < 5) {
    console.log(`Hola ${i}`);
    i++;
}

// Do while
i= 0;
do {
    console.log(`Hola ${i}`);
    i++;
} while (5);

// For
for (let i = 0; i < 5; i++) {
    console.log(`Hola ${i}`);    
}

// for in
let persona = {
    nombre: 'Fabricio',
    apellido: 'Ojeda',
    edad: 25
}
//este ciclo nos ayuda recorrer un objeto obteniendo sus claves
for (let clave in persona) {
    console.log(clave, persona[clave]);
}

// for of
// para objetos iterables
const arreglo = [1, 2, 3, 4, 5];
for (let valor of arreglo) {
    console.log(valor);
}
var nombre = 'Fabricio';
for (const letra of nombre) {
    console.log(letra);
}