/**
 * Funciones
 */

// declarativa

function saludar () {
    console.log(`Hola desde funcion declarativa`);
}
saludar();

let nombre = 'Fabricio Ojeda';

function saludarNombre (nombre) {
    console.log(`Hola ${nombre}`);
}
saludarNombre(nombre);

function devolverSaludo (nombre) {
    return `Hola ${nombre} como estas?`;
}
let saludo = devolverSaludo(nombre);
console.log(devolverSaludo('Hugo Ojeda'),'\n',saludo);

//Expresion o anonimas

// suma es la referencia de memoria donde se guarda la funcion
const suma = function (a = 0, b = 0) {
    return a + b;
}
let resultado = suma(3, 4);
console.log(resultado, suma(11, 10));
console.log(suma(21));

// Flecha o arrow function

const resta = (a, b) => {
    if (typeof a === 'number' && typeof b === 'number') {
        return a - b;
    } else {
        return `Solo puede restar numeros`;
    }
}
resultado = resta(21, '1');
console.log(resultado, resta(11, 3));

//Arrow function contienen un return implicito entonces:
const multiplicar = (a, b) => a * b;
resultado = multiplicar(3, 2);
console.log(resultado, multiplicar(4, 4));