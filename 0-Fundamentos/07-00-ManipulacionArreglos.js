/**
 * Manipulacion de arreglos
 */

// ForEach
const letras = ["a", "d", "e", "b", "c"];

console.log(letras.length); //longitud = 5

letras.sort(); //ordena el arreglo

for (let i = 0; i < letras.length; i++) {
  //indices de 0 a 4
  console.log(letras[i]);
}

//recibe una funcion flecha
letras.forEach(letra => console.log(letra));

//mutabilidad = modifica
// push - shift - pop 

//push - inserta el nuevo elemento al final del arreglo
letras.push('f');
console.log(letras);

//shift - retira y retorna el primer elemento del arreglo
let primerLetra = letras.shift();
console.log(primerLetra);

//pop - retira y retorna el ultimo elemento del arreglo
let ultimaLetra = letras.pop();
console.log(ultimaLetra);
console.log(letras);


