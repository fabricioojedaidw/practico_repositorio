/**
 * map - devuelve un nuevo arreglo y no modifica el original - inmutable
 * 
 * se le pasa una funcion callback por parametro
 * 
 * map debe retornar el elemento modificable
 */

 const estudiantes = ['Fabricio', 'Nabil', 'Tomas', 'Matias'];

 //manipulamos los elementos transformandolos a un objeto de js
 let asistencia = estudiantes.map((nombre) => {
     return {
         nombre: nombre,
         asistencia: false
     }
 });
 console.log(estudiantes);
 
 //ahora tenemos un arreglo de objetos
 console.log(asistencia);
 
 let nombres = estudiantes.map(nombre => `${nombre.toUpperCase()}.`);
 console.log(nombres);
 
 // errores al trabajar con map
 const productos = [
     {nombre: 'camiseta', precio: 15},
     {nombre: 'zapatilla', precio: 20},
     {nombre: 'pantalon', precio: 17}
 ];
 
 //Queremos un nuevo arreglo con los productos y agregarle un impuesto
 
 // let productosImpuestos = productos.map(producto => {
 //     producto.impuesto = .12;
 //     return producto
 //     /**
 //      * aca estamos modificando y retornando el mismo objeto que contiene el array
 //      * entonces se modifica el arreglo original
 //      */
 // });
 // console.log(productos);
 // console.log(productosImpuestos);
 
 let productosImpuestos = productos.map(producto => {
     return {
         ...producto,
         /**
          * Spread operator
          * agarra sus propiedades y las reparte en el nuevo objeto
          */
         impuesto: .12
     }
 });
 console.log(productosImpuestos);
 console.log(productos);
 
 //solo precios de los productos
 let precios = productos.map(producto => producto.precio);
 console.log(precios);