/**
 * Filter
 * Inmutable
 * 
 * Filtra los elementos de un arreglo en base a una condicion logica
 */

 const students = [
    {nombre: 'Fabricio', edad: 25, matriculado: true},
    {nombre: 'Nabil', edad: 25, matriculado: true},
    {nombre: 'Tomas', edad: 22, matriculado: true},
    {nombre: 'Franco', edad: 27, matriculado: false},
];

let filtrados = students.filter(estudiante => estudiante.edad >= 25 && estudiante.matriculado);
console.log(filtrados);

// si se tiene un arreglo con muchos registros no conviene hacer un filtrado en JS
// este filtrado se deberia hacer en la BDD
