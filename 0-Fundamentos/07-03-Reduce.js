/**
 * reduce
 *
 * reducir el array a un solo valor string/boolean/number/object
 */

// reduce( (acumulador/aRetornar, elemento) => {reduccion, estado inicial de acumulador});
//necesita los dos parametros indicados

//Ej: Necesitamos obtener el promedio de calificaciones de un estudiante
const calificaciones = [4, 6, 8, 4, 9];
let suma = calificaciones.reduce(
  (total, calificacion) => (total += calificacion),
  0
);
console.log(calificaciones);
console.log(suma);
console.log(`Promedio: ${suma / calificaciones.length}`);

//Ej: Contar la cantidad de veces que se repiten las edades
const edades = [21, 21, 23, 43, 21, 43, 18, 18, 23, 23];
let resuEdades = edades.reduce((acumulador, edad) => {
  if (!acumulador[edad]) {
    acumulador[edad] = 1;
  } else {
    acumulador[edad] += 1;
  }
  return acumulador;
}, {});
console.log(edades);
console.log(resuEdades);

//Calcular el valor total de las ventas de cada procucto
const ventas = [
  { nombre: "camiseta", precio: 15, totalVendido: 10 },
  { nombre: "zapatilla", precio: 150, totalVendido: 8 },
  { nombre: "pantalon", precio: 20, totalVendido: 30 },
];

let resultadoVentas = ventas.reduce((acumulador, producto) => {
  let totalVentas = producto.precio * producto.totalVendido;
  acumulador[producto.nombre] = `$${totalVentas}`;
  return acumulador;
}, {});
console.log(ventas);
console.log(resultadoVentas);

//Calcular la cantidad de estudiantes matriculados y no matriculados
let matriculados = students
  .map((estudiante) => estudiante.matriculado) //obtenemos un arreglo con los valores de matriculado o no matriculado
  .reduce(
    (acumulador, item) => {
      //reducimos el arreglo obtenido anteriormente
      if (item) {
        acumulador.cantMatriculados += 1;
      } else {
        acumulador.cantNoMatriculados += 1;
      }
      return acumulador;
    },
    { cantMatriculados: 0, cantNoMatriculados: 0 }
  );
console.log(students);
console.log(matriculados);


const mascotas = [
    { nombre: 'Pelusa', edad: 12, tipo: 'gato' },
    { nombre: 'Puchini', edad: 12, tipo: 'perro' },
    { nombre: 'Kikin', edad: 3, tipo: 'perro' },
    { nombre: 'Vieja', edad: 2, tipo: 'gato' },
];

//Para no utilizar un find en un arreglo que tiene 1 millon de elementos podemos indexar
//este arreglo para despues acceder a un elemento deseado de una manera mas facil

const mascotasIndexadas = mascotas.reduce((acc, el) => ({
    ...acc,
    [el.nombre]: el,
}), {});
// console.log(mascotasIndexadas);
// console.log(mascotasIndexadas['Kikin']);

const anidado = [1, [2, 3], 4, [5, 1, 3]];
const desanidado = anidado.reduce((acc, el) => acc.concat(el), []);
console.log(desanidado);