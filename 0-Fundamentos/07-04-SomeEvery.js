'use strict';
/**
 * some
 * 
 * si alguno de los elementos de un arreglo cumple con una condicion especifica
 */
 const esPar = numero => numero % 2 == 0; 

const numeros = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
const impares = [1, 3, 5, 7, 9];
const pares = [2, 4, 6, 8, 10];
const mesclados = [...pares, 3];

var resultado = pares.some( numero => esPar(numero));
console.log(resultado);

resultado = impares.some( numero => esPar(numero));
console.log(resultado);

resultado = mesclados.some( numero => esPar(numero));
console.log(resultado);

/**
 * every
 * 
 * si todos los elementos de un arreglo cumplen con una condicion especifica
 */
resultado = pares.every( numero => esPar(numero));
console.log(resultado);

resultado = impares.every( numero => esPar(numero));
console.log(resultado);

resultado = mesclados.every( numero => esPar(numero));
console.log(resultado);

//Verificar si un arreglo de personas todos son mayores de edad
const personas = [
    {nombre: 'Fabricio', edad: 25},
    {nombre: 'Morena', edad: 15},
    {nombre: 'Lucas', edad: 21},
    {nombre: 'Julieta', edad: 17},
];

var todosMayoresEdad = personas
    .map( persona => persona.edad)
    .every( edad => edad >= 18);
console.log(todosMayoresEdad);

//Nombres de los mayores de edad
const nombresMayoresEdad = personas
    .filter( persona => persona.edad >= 18)
    .map( persona => persona.nombre);
console.log(nombresMayoresEdad);

//Practicando map y reduce
//Cantidad mayores y menores
const cantMayoresMenores = personas
    .map(persona => persona.edad)
    .reduce((acumulador, edad) => {
        if (edad >= 18) {
            acumulador.cantMayores += 1;
        } else {
            acumulador.cantMenores += 1;
        }
        return acumulador;
    }, {
        cantMayores: 0,
        cantMenores: 0
    });
console.log(cantMayoresMenores);

