/**
 * find 
 * 
 * inmutable
 * 
 * busca y retorna el primer elemento que cumpla con cierta condicion
 * retorna undefined si no encuentra
 */
const clientes = [
    {id: 1, nombre: 'Fabricio'},
    {id: 2, nombre: 'Juan'},
    {id: 3, nombre: 'Romina'},
    {id: 4, nombre: 'Julieta'},
    {id: 1, nombre: 'Pablo'}, //tiene id 1 para ver las diferencias entre filter y find
];
var cliente = clientes.find(cliente => cliente.id === 1);
console.log(cliente);

//La diferencia entre find y filter es que find retorna el primer elemento que cumpla con la condicion
//y filter nos retorna un arreglo de los elementos que cumplan con la condicion

var clientesFiltrados = clientes.filter(cliente => cliente.id === 1);
console.log(clientesFiltrados);

/**
 * findIndex
 * 
 * retorna la posicion del primer elemento que cumpla con cierta condicion
 */

var posicionId2 = clientes.findIndex(cliente => cliente.id === 2);
console.log(posicionId2);

var clienteId2 = clientes[posicionId2]; 
console.log(clienteId2);
