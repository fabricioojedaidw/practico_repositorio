/**
 * includes
 * 
 * determina si en un arreglo existe un elemento especifico
 */

const mascotas = ['perro', 'gato', 'conejo'];
var resultado = mascotas.includes('perro');
console.log(resultado);

console.log('fabricio'.includes('f'));

//Ej

const buscador = (elemento) => {
    let clientes = [
        {id: 1, nombre: 'Fabricio'},
        {id: 2, nombre: 'Juan'},
        {id: 3, nombre: 'Romina'},
        {id: 4, nombre: 'Julieta'},
        {id: 5, nombre: 'Pablo'}, //tiene id 1 para ver las diferencias entre filter y find
    ];

    return clientes.filter(cliente => cliente.nombre.includes(elemento));
}

console.log(buscador('br'));