/**
 * join
 *
 * une todos los elementos del arreglo y genera un string a partir de esta union
 *
 * puede o no recibir parámetros
 *
 * pasamos por parametro el caracter por cual queremos separar los elementos del arreglo
 */

const elementos = ["aire", "fuego", "agua"];
var resultado = elementos.join();
console.log(resultado);

resultado = elementos.join("/");
console.log(resultado);

/**
 * join es muy utilizado para generar archivos CSV
 */

//Ej si utiliamos join con un arreglo
var clientes = [
  { id: 1, nombre: "Fabricio" },
  { id: 2, nombre: "Juan" },
  { id: 3, nombre: "Romina" },
  { id: 4, nombre: "Julieta" },
  { id: 5, nombre: "Pablo" },
];
// console.log(clientes.join());

//Ej de generar un CSV con el arreglo clientes

console.log(Object.values({ id: 1, nombre: "Fabricio" }));
console.log(Object.keys({ id: 1, nombre: "Fabricio" }));

const csvGenerator = (array, separator = ',') => {
    let csv = '';
    let headers = Object.keys(array[0]).join(separator);
    let data = array
        .map(element => Object.values(element).join(separator));
    csv = headers;
        data.forEach(element => {
        csv += `\n${element}`;
    });
    return csv;
}
console.log(csvGenerator(clientes));


