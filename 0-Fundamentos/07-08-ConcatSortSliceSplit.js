/**
 * concat
 * 
 * concatenar arreglos
 */
const array1 = [1, 2, 3, 4, 5];
const array2 = [6, 7, 8, 9, 10];
const array3 = array1.concat(array2);

console.log(array1);
console.log(array2);
console.log(array3);

const array4 = [...array1, ...array2];
console.log(array4);

/**
 * sort
 * 
 * ordena ascendentemente un arreglo en base al código ascii
 */
const array5 = [2, 3, 4, 1, 7, 5, 8, 9, 6];
console.log(array5.sort());

const meses = ['Dic', 'Ene', 'Mar', 'Feb'];
console.log(meses.sort());

//Los números los sigue ordenando segun el código ascii
const numeros = [1, 10000, 21, 30, 5];
console.log(numeros.sort());

/**
 * ((a,b) = > a - b)  
 * a = primer elemento 
 * b = segundo elemento
 * 
 * realiza la resta entre ellos y sabe cual es el menor
 * para ordenar ascendentemente
 * 
 * para ordenar descendentemente b - a 
 */
var numerosOrdenados = numeros.sort((a, b) => a - b);
console.log(numerosOrdenados);

numerosOrdenados = numeros.sort((a, b) => b - a);
console.log(numerosOrdenados);



/**
 * splice - mutable (modifica el arreglo original)
 * 
 * remover o modificar elementos de un array o el array en si
 * 
 * parametros (posicion a comenzar, cantidad a eliminar, elemento reemplazante)
*/

const nombres = ['Fabricio', 'Nabil', 'Tomas', 'Mauricio'];
nombres.splice(1, 1, 'Matias'); 
console.log(nombres);


/**
 * slice
 * 
 * retorna una copia de una parte indicada del arreglo original
 * 
 * parametros (inicio, posicion final(no incluyente))
 */

const resultado = nombres.slice(1,3); //no se incluye la posicion 3 del arreglo
console.log(resultado);
