/**
 * Asincronismo
 * 
 * JS ejecuta el código en un solo proceso
 * entonces si necesitamos realizar un proceso demasiado extenso
 * por ejemplo traer datos desde la base de datos esto puede detener todo 
 * el resto del flujo del programa hasta que se termine este proceso
 * 
 * para esto existe el asincronismo, realizando subprocesos que trabajan
 * por "atras" y de esta manera no interrumpe el flujo del programa principal
 * 
 * a este modelo de concurrencia se conoce como "no bloqueante"
 * 
 * "event loop"
 */

console.log('tarea 1');
console.log('tarea 2');
console.log('tarea 3');
//setTimeout = asincrono
setTimeout(() => {
console.log('tarea 4');
}, 2000);
console.log('tarea 5');
