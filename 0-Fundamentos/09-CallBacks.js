/**
 * CallBacks
 * 
 * funciones que son pasadas como parametros a otras funciones para que estas
 * una vez finalizadas sus tareas ejecuten esta funcion callback
 */

//Forma inicial de JS de gestionar las formas asincronas
const suma = (a, b, cb) => cb(a + b);
const imprimir = data => console.log(data);
suma(1, 2, imprimir);

//Ejemplo
//cuando termina de traer los datos (3 seg en setTimeout) los retorna en la funcion callback
const getData = (cb, cbError) => {
    if (false) {
        setTimeout(() => {
            cb({
                nombre: 'Fabricio',
                apellido: 'Ojeda'
            })
        }, 3000);    
    } else {
        cbError(new Error('No se pudo obtener los datos'));
    }
    
}
const imprimirData = data => console.log(data);
const errorHandler = (error) => console.log(error);

getData(imprimirData, errorHandler);