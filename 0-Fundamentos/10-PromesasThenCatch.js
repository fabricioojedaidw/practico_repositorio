/**
 * Promesas - Promises
 * 
 * ayudan a controlar funciones asincronas
 * y ayuda a comprobar si se resuelven adecuadamente
 * o sucedio un error
 */


// getData1 retorna una nueva promesa (tiene return implicitamente)
// podemos pasarle parametros
const getData1 = (error) => new Promise((res, rej) => {
    if (!error) {
        // si no hay error simulamos que se conecta al servidor y demora 3 seg
        setTimeout(() => {
            res(
                // retorna los datos para ser gestionada
                {
                    nombre: 'Fabricio',
                    apellido: 'Ojeda'
                }
            );
        }, 3000);
    } else {
        rej(
            // retorna el error para ser gestionado
            'No pudimos obtener los datos'
        );
    }
});


//Formas de forma de resolver las promesas

// .then .catch
// console.log(`inicio`);

// getData1(false)
//     .then((data) => {
//         console.log(data);
//     })
//     .catch((error) => {
//         console.log(error);
//     });

//     console.log(`fin`);


//getData2 es una nueva promesa
// no se puede pasarle parametros
// const getData2 = new Promise((resolve, reject) => {});

const suma = new Promise((res, rej) => {
    if (true) {
        res(); //no puede resolver la suma ya que al ser una promesa no puede tener parametros
    }
});


/**
 * Encadenar promesas
 * 
 * se deben encadenar en en .then
 */
const getData2 = (error) => new Promise((res, rej) => {
    if (!error) {
        // si no hay error simulamos que se conecta al servidor y demora 3 seg
        setTimeout(() => {
            res(
                // retorna los datos para ser gestionada
                {
                    nombre: 'Rocio',
                    apellido: 'Quinteros'
                }
            );
        }, 3000);
    } else {
        rej(
            // retorna el error para ser gestionado
            'No pudimos obtener los datos de data2'
        );
    }
});

console.log(`inicio`);

getData1(false)
    .then((data) => {
        console.log(data);
        return getData2(true); //encadena data2 a data1
    })
    .then((data) => { // maneja el resolve de data2
        console.log(data);
    })
    .catch((error) => { //este catch puede resolver los 2 errores
        console.log(error);
    });

    console.log(`fin`);