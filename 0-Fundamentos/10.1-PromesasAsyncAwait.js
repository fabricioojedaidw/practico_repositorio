/**
 * Async - Await
 * son palabras reservadas
 * 
 * otra forma de resolver promesas
 * 
 * es insertaron ya que .then y .catch
 * aveces generan complicaciones para leer el codigo
 * y generan muchas lineas de codigo
 * 
 * debe estar async para poder utilizar await
 */

 const getData = (error) => new Promise((res, rej) => {
    if (!error) {
        // si no hay error simulamos que se conecta al servidor y demora 3 seg
        setTimeout(() => {
            res(
                // retorna los datos para ser gestionada
                {
                    nombre: 'Fabricio',
                    apellido: 'Ojeda'
                }
            );
        }, 3000);
    } else {
        rej(
            // retorna el error para ser gestionado
            'No pudimos obtener los datos'
        );
    }
});

const getData2 = (error) => new Promise((res, rej) => {
    if (!error) {
        // si no hay error simulamos que se conecta al servidor y demora 3 seg
        setTimeout(() => {
            res(
                // retorna los datos para ser gestionada
                {
                    nombre: 'Rocio',
                    apellido: 'Quinteros'
                }
            );
        }, 3000);
    } else {
        rej(
            // retorna el error para ser gestionado
            'No pudimos obtener los datos de data2'
        );
    }
});


const main = async () => {
    try {
        let resultado = await getData(false);
        console.log(resultado);
        let resultado2 = await getData2(true);
        console.log(resultado2);
    } catch (error) {
        console.log(error);
    }
}

main();