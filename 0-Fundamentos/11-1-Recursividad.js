/**
 * Si no utilizamos trampoline
 * llega un momento que se llena la pila y no se termina de resolver los llamados
 */
const trampoline = fn => (...args) => {
    let result = fn(...args);
    while ( typeof result === 'function' ) {
        result = result();
    }
    return result;
}

const suma = (number, sum = 0) => {
    if ( number === 0 ) {
        return sum;
    } else {
        return  () => suma( number - 1, sum + number);   
    }
}

const tsuma = trampoline(suma);
const res = tsuma(100);
console.log(res);