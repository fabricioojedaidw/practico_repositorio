const csvGenerator = (array, separator = ',') => {
    let csv = '';
    let headers = Object.keys(array[0]).join(separator);
    let data = array
        .map(element => Object.values(element).join(separator));
    csv = headers;
        data.forEach(element => {
        csv += `\n${element}`;
    });
    return csv;
}