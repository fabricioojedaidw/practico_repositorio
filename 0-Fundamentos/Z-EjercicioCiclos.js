//Tablas del 2 al 12

// for
for (let i = 2; i <= 12; i++ ) {
    for (let j = 1; j <= 10; j++) {
        console.log(`${j} * ${i} = ${j*i}`);
    }
}

// while
let i = 2, j = 1;
while (i <= 12) {
    j = 1;
    while (j <= 10) {
        console.log(`${j} * ${i} = ${j*i}`);
        j++;
    }
    i++;
}

// do while
i = 2;
do {
    j = 1;
    do {
        console.log(`${j} * ${i} = ${j*i}`);
        j++
    } while (j <= 10);
    i++
} while (i <= 12);