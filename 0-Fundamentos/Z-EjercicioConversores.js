const segundosHora = horas => {
  const SEG_MIN = 60;
  const MIN_HORA = 60;
  return SEG_MIN * MIN_HORA * horas;
};
console.log(segundosHora(24));

const gradosToFahrenheit = grados => {
    return grados * (9/5) + 32;
}
console.log(gradosToFahrenheit(20));
