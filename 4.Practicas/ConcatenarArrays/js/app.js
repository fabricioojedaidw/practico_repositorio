'use strict';

function cargarArray () {
    let arreglo = [];
    for (let i = 0; i < 5; i++) {
        arreglo.push(getRandomInt(1, 100));
    }
    return arreglo;
}

function cargarEjemplo1 () {
    let array1 = cargarArray();
    document.getElementById('array1').value = array1.join(', ');
}

function cargarEjemplo2 () {
    let array2 = cargarArray();
    document.getElementById('array2').value = array2.join(', ');
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function reiniciar () {
    document.getElementById('array1').value = '';
    document.getElementById('array2').value = '';
    document.getElementById('resultado').innerHTML = '';
}

function concatenarArrays () {
    let resultado = '';
    let array1 = document.getElementById('array1')
        .value.split(/,/)
        .filter(element => !(isNaN(element)));
    let array2 = document.getElementById('array2').value
        .split(/,/)
        .filter(element => !(isNaN(element)));
    let array3 = [];
    if (array1[0] !== '' && array2[0] !== '') {
        if (array1.length >= array2.length) {
            for (let i = 0; i < array1.length; i++) {
                array3.push(array1[i]);
                array3.push(array2[i]);
            }
        } else if (array2.length > array1.length) {
            for (let i = 0; i < array2.length; i++) {
                array3.push(array1[i]);
                array3.push(array2[i]);
            }
        }
        array3 = array3.filter(element => !(isNaN(element)));
        resultado = `[${array3.join(', ')}]`;
        document.getElementById('resultado').innerHTML = resultado;
    }
    return array3;
}