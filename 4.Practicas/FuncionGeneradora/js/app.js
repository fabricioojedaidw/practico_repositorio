'use strict';

const generator = counter(0);
const numeros = document.getElementById('numeros');
var enEjecucion = false;
var intervalId;

function* counter(value) {
    while (true) {
        yield value++;
    }
}

function iniciar () {
    if (!enEjecucion) {
        enEjecucion = true;
        intervalId = setInterval(() => {
            numeros.innerHTML += `${generator.next().value}, `; 
        }, 1000);
    }
}

function parar () {
    enEjecucion = false;
    clearInterval(intervalId);
}