/**4. 
 * Haga un algoritmo para una función que reciba dos parámetros N y K y que retorne los K dígitos más a la izquierda de N. 
 * Por ejemplo, extraerDigitos (542207, 2) debe retornar 54. */
const extraerDigitos = (n, k) => {
    if (! (isNaN(n) || isNaN(k))) {
        if (String(n).length >= k && k > 0) {
            return String(n).substring(0, k);
        }
        return `No se puede extraer esa cantidad de digitos`;
    }
    return `Ingrese numeros`;
}
console.log(extraerDigitos(542207, 2));