/**6.
 * Escribir un programa que pida números positivos al usuario.
 * Mostrar el número cuya sumatoria de dígitos fue mayor y la cantidad de números cuya sumatoria de dígitos fue menor que 10.
 * Utilizar una o más funciones, según sea necesario. */
const numeros = [3, 1, 21, 11, 56, 98];
console.log(Math.max(...numeros));

const sumatoriaDigitos = numeros.map((numero) => {
  let resultado = String(numero)
    .split("")
    .reduce((acc, element) => (acc += Number(element)), (acc = 0));
  return {
    numero: numero,
    sumatoria: resultado,
  };
});
console.log(sumatoriaDigitos);

const menorADiez = arreglo => arreglo.filter(element => element.sumatoria < 10);
console.log(menorADiez(sumatoriaDigitos));

const mayorSumatoria = arreglo => arreglo.reduce((acc, element) => {
    if (element.sumatoria >= acc.sumatoria) {
        acc = element;
    }
    return acc;
} , {numero: 0, sumatoria: 0});
console.log(mayorSumatoria(sumatoriaDigitos));