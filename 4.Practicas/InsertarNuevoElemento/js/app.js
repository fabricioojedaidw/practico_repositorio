'use strict';

function cargarArray () {
    let arreglo = [];
    for (let i = 0; i < 5; i++) {
        arreglo.push(getRandomInt(1, 100));
    }
    return arreglo;
}
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function cargarEjemplo () {
    let arreglo = cargarArray();
    document.getElementById('arreglo').value = arreglo.join(', ');
    cargarPosiciones();
}

function cargarPosiciones () {
    let arreglo = obtenerArreglo();
    let opciones = ``;
    for (let i = 0; i < arreglo.length; i++) {
        opciones += `<option value="${i}">${i}</option>`;
    }
    document.getElementById('posicion').innerHTML = opciones;
}

function obtenerArreglo () {
    let arreglo = document.getElementById('arreglo').value.split(/,/)
    .filter(element => !(isNaN(element)));
    return arreglo;
}

function insertar () {
    let nuevoNumero = document.getElementById('valor').value;
    let posicion = document.getElementById('posicion').value;
    let arreglo = obtenerArreglo();
    arreglo[posicion] = ` ${nuevoNumero}`;
    document.getElementById('arreglo').value = arreglo.join(',');
}

function reiniciar () {
    document.getElementById('arreglo').value = '';
    document.getElementById('posicion').innerHTML = '';
    document.getElementById('valor').value = '';
}