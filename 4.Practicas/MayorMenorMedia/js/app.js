'use strict';
var valores = [];

// BOTONES

function cargarValores () {
    for (let i = 0; i < 5; i++) {
        valores[i] = getRandomInt(1, 100);
    }
    document.getElementById('valores').value = valores.join(',');
    calcular();
}

function calcular () {
    let contenido = document.getElementById('valores').value.split(/,/);
    valores = contenido.filter(element => !(isNaN(element)));
    if (contenido[0] !== '') {
        let mayor = calcularMayor();
        let menor = calcularMenor();
        let suma = sumatoria();
        let media = promedio();
        let resultado =
        `
        Sumatoria: ${suma}<br/>
        Media: ${media}<br/>
        Mayor: ${mayor}<br/>
        Menor: ${menor}`;
        document.getElementById('resultado').innerHTML = resultado;
    }
}

function reiniciar () {
    document.getElementById('valores').value = '';
    document.getElementById('resultado').innerHTML = '';
    valores = [];
}

// CALCULOS

function calcularMayor() {
    let mayor = parseInt(valores[0]);
    for (let i = 1; i < valores.length; i++) {
        if (parseInt(valores[i]) > mayor) {
            mayor = valores[i];
        }
    }
    return mayor;
}

function calcularMenor() {
    let menor = parseInt(valores[0]);
    for (let i = 1; i < valores.length; i++) {
        if (parseInt(valores[i]) < menor) {
            menor = valores[i];
        }
    }
    return menor;
}

function sumatoria () {
    let suma = 0;
    for (let i = 0; i < valores.length; i++) {
        if (!(isNaN(valores[i]))) {
            suma += parseInt(valores[i]);
        }
    }
    return suma;
}

function promedio () {
    let media = sumatoria() / valores.length;
    return media;
}

// OTROS

c
