/**
 * Escribir un programa que almacene las asignaturas de un curso (por ejemplo, Matemáticas, Física, Química, Historia y Lenguaje) en un arreglo, 
 * pregunte al usuario la nota que ha sacado en cada asignatura y elimine de la lista las asignaturas aprobadas. 
 * Al final, el programa debe mostrar por pantalla las asignaturas que el usuario tiene que repetir.
 */
var notas = [
    {materia: 'Matematicas', nota: 8},
    {materia: 'Fisica', nota: 5},
    {materia: 'Quimica', nota: 7},
    {materia: 'Historia', nota: 9},
    {materia: 'Lenguaje', nota: 4},
];
notas = notas.filter(asignatura => asignatura.nota < 6);
console.log(notas);