/**
 * 2. Escribir un programa que pida al usuario una palabra y muestre por pantalla si es un palíndromo. 
 * Una palabra es un palíndromo si se escribe de igual manera al derecho y a revés.
 */
let palabra = 'neuquen';
const esPalindromo = palabra => {
    let alReves = palabra.split('').reverse().join('');
    return alReves === palabra;
}
console.log(esPalindromo(palabra));