/**
 * Escribir un programa que almacene las siguientes matrices:
𝑨=(𝟐 𝟏𝟎  𝑩=(𝟏𝟓 -9
    𝟔−𝟑);     𝟕−𝟒);
en una lista y muestre por pantalla la matriz resultante al realizar la operación A+B.
Nota: Para representar matrices mediante arreglos, es recomendado utilizar arreglos anidados, es decir, 
los elementos del arreglo serán otros arreglos, representando cada vector una fila en un arreglo.
 */
'use strict';
var matA = [ [2, 10], [6, -3] ];
var matB = [ [15, -9], [7, -4] ];

const sumaMatrices = (a, b) => {
    let resultado = [];
    for (let i = 0; i < a.length; i++) {
        let rowA = a[i];
        let rowB = b[i];
        let suma = [];
        for (let j = 0; j < rowA.length; j++) {
            suma.push(rowA[j] + rowB[j]);
        }
        resultado.push(suma);
    }
    return resultado;
};
console.table(sumaMatrices(matA, matB));
