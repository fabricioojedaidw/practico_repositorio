/**
 * 5. Escribir un programa que simule el lanzamiento de dos dados. Hacer uso de la función random según el lenguaje de programación en el que se
 * encuentre trabajando para obtener números aleatorios entre 1 y 6 para cada uno de los lanzamientos de los dados.
 * Sumar el resultado de lanzar dos dados y anotar en un arreglo el número de apariciones de dicha suma, repitiendo 36.000 veces esta operación.
 */

function lanzarDado() {
  const min = 1,
    max = 7;
  return Math.floor(Math.random() * (max - min)) + min;
}
const resultados = [];
var i = 1;
while (i <= 36000) {
  resultados.push(lanzarDado() + lanzarDado());
  i++;
}
var resultadosUnicos = [...new Set(resultados)].sort((a, b) => a - b);
resultadosUnicos = resultadosUnicos.map((resultado) => {
  let contador = resultados.reduce((cont, element) => {
    if (element === resultado) {
      cont++;
    }
    return cont;
  }, (cont = 0));
  return {
      resultado: resultado,
      repeticiones: contador
  }
});
console.log(resultadosUnicos);