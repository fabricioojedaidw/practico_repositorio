/**
 * 1. Realizar un programa que conste de una clase llamada Alumno que tenga como atributos el nombre y la nota del alumno. Definir los métodos 
 * para inicializar sus atributos (método constructor), imprimirlos y mostrar un mensaje con el resultado de la nota y si ha aprobado o no.
 */
class Alumno {
    constructor (nombre, apellido, nota) {
        this._nombre = nombre;
        this._apellido = apellido;
        this._nota = nota;
    }
    get nombre () {
        return this._nombre;
    }
    set nombre (nombre) {
        this._nombre = nombre;
    }
    get apellido () {
        return this._apellido;
    }
    set apellido (apellido) {
        this._apellido = apellido;
    }
    get nota () {
        return this._nota;
    }
    set nota (nota) {
        this._nota = nota;
    }
    toString () {
        return `Nombre y apellido: ${this._nombre} ${this._apellido}, Nota: ${this._nota}`;
    }
    resultado () {
        let calificacion = `Nota: ${this._nota}`;
        if (this._nota >= 6) {
            calificacion += ` APROBADO`;
        } else {
            calificacion += ` DESAPROBADO`;
        }
        return calificacion;
    }
}
var unAlumno = new Alumno ('Fabricio', 'Ojeda', 9);
console.log(unAlumno.toString());
console.log(unAlumno.resultado());