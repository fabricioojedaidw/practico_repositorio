/**
 * 2. Realizar una clase que administre una agenda. Se debe almacenar para cada contacto el nombre, el teléfono y el email. 
 * Además, deberá mostrar un menú con las siguientes opciones:
• Añadir contacto
• Lista de contactos
• Buscar contacto
• Editar contacto
• Cerrar agenda
 */
"use strict";
class Contacto {
  constructor(nombre, telefono, email) {
    this._nombre = nombre;
    this._telefono = telefono;
    this._email = email;
  }
  get nombre() {
    return this._nombre;
  }
  set nombre(nombre) {
    this._nombre = nombre;
  }
  get telefono() {
    return this._telefono;
  }
  set telefono(telefono) {
    this._telefono = telefono;
  }
  get email() {
    return this._email;
  }
  set email(email) {
    this._email = email;
  }
  toString() {
    return `Nombre: ${this._nombre}, Telefono: ${this._telefono}, Email: ${this._email}`;
  }
}

class Agenda {
  contactos = [];
  constructor(propietario) {
    this._propietario = propietario;
  }
  get propietario() {
    return this._propietario;
  }
  set propietario(propietario) {
    this._propietario = propietario;
  }
  nuevoContacto(nombre, telefono, email) {
    let contacto = new Contacto(nombre, telefono, email);
    if (this.contactos.length === 0) {
      this.contactos.push(contacto);
      return `Se agrego el primer contacto de la agenda`;
    } else {
      let index = this.contactos.findIndex(
        (unContacto) => unContacto.telefono === contacto.telefono
      );
      if (index === -1) {
        this.contactos.push(contacto);
        return `Se agrego el nuevo contacto`;
      } else {
        return `Ya existe un contacto con el numero ${contacto.telefono}`;
      }
    }
  }
  buscarContacto(buscado) {
    let resultado = this.contactos.find(
      (contacto) =>
        contacto.nombre === buscado ||
        contacto.telefono === buscado ||
        contacto.email === buscado
    );
    return resultado;
  }
  editarContacto(buscado, nombre = "", telefono = "", email = "") {
    let aEditar = this.buscarContacto(buscado);
    if (aEditar !== undefined) {
      let index = this.contactos.findIndex(
        (contacto) => contacto.telefono === aEditar.telefono
      );
      if (nombre !== "") {
        aEditar.nombre = nombre;
      }
      if (telefono !== "") {
        aEditar.telefono = telefono;
      }
      if (email !== "") {
        aEditar.email = email;
      }
      this.contactos[index] = aEditar;
      return aEditar;
    } else {
      return `No se encontro el contacto a editar`;
    }
  }
  listaContactos() {
    let listado = ``;
    for (let i = 0; i < this.contactos.length; i++) {
      listado += `${i + 1} ${this.contactos[i].toString()}\n`;
    }
    return listado;
  }
}
var miAgenda = new Agenda("Fabricio Ojeda");
let res = miAgenda.nuevoContacto("Rodrigo", "155678123", "rodri@gmail.com");
res = miAgenda.nuevoContacto("Pol", "154456123", "pol@gmail.com");
res = miAgenda.nuevoContacto("Matu", "15445777", "matu@gmail.com");

// console.log(res);
// console.log(miAgenda.buscarContacto("Rodrigo"));
// console.log(miAgenda.listaContactos());
// console.log(miAgenda.editarContacto("Pol", "Sebastian Alessio", "", ""));
