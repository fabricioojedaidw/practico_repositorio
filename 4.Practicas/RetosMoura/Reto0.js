/**
 * Reto 0 FÁCIL
 * * Enunciado: Escribe un programa que muestre por consola (con un print) los números de 1 a 100 (ambos incluidos y con un salto de línea entre cada impresión), sustituyendo los siguientes:
 * - Múltiplos de 3 por la palabra "fizz".
 * - Múltiplos de 5 por la palabra "buzz".
 * - Múltiplos de 3 y de 5 a la vez por la palabra "fizzbuzz".
 */

const fizzBuzz = (fb) => {
  const numeros = [];
  for (let i = 0; i < 100; i++) {
    numeros[i] = i + 1;
  }
  let multiplos = [];
  switch (fb) {
    case "fizz":
      multiplos = numeros.map((x) => (x % 3 === 0 ? (x = "fizz") : x));
      break;
    case "buzz":
      multiplos = numeros.map((x) => (x % 5 === 0 ? (x = "buzz") : x));
      break;
    case "fizzbuzz":
      multiplos = numeros.map((x) =>
        x % 3 === 0 && x % 5 === 0 ? (x = "fizzbuzz") : x
      );
      break;
    default:
      multiplos = numeros;
      0;
      break;
  }
  return multiplos;
};
const arreglo = fizzBuzz('fizzbuzz');
console.log(arreglo);