/**
 * Reto 1 MEDIA
 *  * Enunciado: Escribe una función que reciba dos palabras (String) y retorne verdadero o falso (Boolean) según sean o no anagramas.
 * Un Anagrama consiste en formar una palabra reordenando TODAS las letras de otra palabra inicial.
 * NO hace falta comprobar que ambas palabras existan.
 * Dos palabras exactamente iguales no son anagrama.
 */
const anagram = (string1, string2) => {
    if (string1 === string2 || string1.length !== string2.length) {
        return false;
    }
    let aux = '';
    const arreglo = [...string1];
    arreglo.forEach(letter => {
        if (string2.includes(letter)) {
            let ind = string2.search(letter);
            aux += string2[ind];
           }
    });
    if (string1 === aux) {
        return true;
    } else {
        return false;
    }
}
if (anagram('halo','hola')) {
    console.log('Es anagrama');
} else {
    console.log('NO es anagrama!!!');
}

