/**
 * Reto 3 MEDIA
 *  * Enunciado: Escribe un programa que se encargue de comprobar si un número es o no primo.
 * Hecho esto, imprime los números primos entre 1 y 100.
 */
const primo = (x, i=2) => {
    if (x === 1) {
        return true;
    } else if (x !== i) {
        if ( (x % i !== 0) && (x % x === 0)) {
            return true && primo(x, i + 1);
        } else{
            return false;
        }
    }
    return true
}
console.log(primo(21));