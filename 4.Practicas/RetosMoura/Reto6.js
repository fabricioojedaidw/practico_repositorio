/**
 * Reto 6 FÁCIL
 *  * Enunciado: Crea un programa que invierta el orden de una cadena de texto sin usar funciones propias del lenguaje que lo hagan de forma automática.
 * - Si le pasamos "Hola mundo" nos retornaría "odnum aloH"
 */

 const invertirCadena = (cadena, invertida = '') => {
    if (cadena.length === 0 || cadena.length === 1) {
        return invertida;
    }
    let letra = cadena[cadena.length-1];
    cadena = cadena.substring(cadena[0], cadena.length-1);
    return invertirCadena(cadena, invertida += letra);
}
console.log(invertirCadena('Fabricio Ojeda'));