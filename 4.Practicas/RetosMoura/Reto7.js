/**
 * Reto 7 MEDIA
 * * Enunciado: Crea un programa que cuente cuantas veces se repite cada palabra y que muestre el recuento final de todas ellas.
 * - Los signos de puntuación no forman parte de la palabra.
 * - Una palabra es la misma aunque aparezca en mayúsculas y minúsculas.
 * - No se pueden utilizar funciones propias del lenguaje que lo resuelvan automáticamente.
 */
const contarPalabras = (texto = "") => {
  let textoArray = texto.toLowerCase().trim().split(" ");
  let palabrasUnicas = [...new Set(textoArray)];
  let resultado = palabrasUnicas.map((element) => {
    let contador = 0;
    for (let i = 0; i < textoArray.length; i++) {
        if (element === textoArray[i]) {
            contador++;
        }
    }
    return {
        palabra: element,
        repeticiones: contador
    };
  });
  return resultado;
};

var texto = "Hola como estas hola estas estas ESTAS hola sale";
console.log(contarPalabras(texto));
