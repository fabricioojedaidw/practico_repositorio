/*
 * Reto #8
 * DECIMAL A BINARIO
 * Dificultad: FÁCIL
 *
 * Enunciado: Crea un programa se encargue de transformar un número decimal a binario sin utilizar funciones propias del lenguaje que lo hagan
 * directamente.
 *
 */

const decimalToBinary = (num) => {
  let resultado = ``;
  let aux = num;
  let negativo = num < 0 ? true : false;
  if (negativo) {
    num *= -1;
  }
  while (num !== 0) {
    aux = Math.trunc(num / 2);
    resultado += `${num % 2}`;
    num = aux;
  }
  resultado = resultado.split("").reverse().join("");
  if (negativo) {
    return `-${resultado}`;
  }
  return resultado;
};
const decimalAbinario = (num) => {
  if (num === 0) {
    return 0;
  }
  if (num % 1 == 0) {
    return decimalToBinary(num);
  } else {
    let div = String(num).split(".");
    let entero = decimalToBinary(Number(div[0]));
    let decimal = Number(`0.${div[1]}`);
    let resultado = "";
    let presicion = 0;
    let cont = true;
    while (cont) {
      resultado += `${Math.trunc(decimal * 2)}`;
      decimal * 2 !== 1
        ? (decimal = Number(`0.${String(decimal * 2).split(".")[1]}`))
        : (decimal = decimal * 2);
      presicion++;
      decimal === 1 || presicion === 4 ? (cont = false) : (cont = true);
    }
    return `${entero}.${resultado}`;
  } 
};
console.log(decimalAbinario(10));
