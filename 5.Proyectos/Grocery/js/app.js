class Grocery {
  static countGrocerys = 0;
  constructor(title) {
    this._id = ++Grocery.countGrocerys;
    this._title = title;
  }
  get id() {
    return this._id;
  }
  get title() {
    return this._title;
  }
  set title(title) {
    this._title = title;
  }
  toString() {
    return `id: ${this._id}, title: ${this._title}`;
  }
}
const groceries = [];
// HTML elements
const form = document.querySelector(".grocery-form");
const textGrocery = document.getElementById("grocery");
const alertMessage = document.querySelector(".alert");
const groceryContainer = document.querySelector(".grocery-container");
const submitBtn = document.querySelector(".submit-btn");
const groceryList = document.querySelector(".grocery-list");
form.addEventListener("submit", addGrocery);

const clearItemsBtn = document.querySelector(".clear-btn");
clearItemsBtn.addEventListener("click", () => {
  groceryContainer.classList.remove("show-container");
  groceries.length = 0;
  groceryList.innerHTML = "";
  textGrocery.value = "";
  modifyAlert("alert-danger", "Empty list");
});

// edit option
let editElement;
let editFlag = false;
let editID = "";

function addGrocery(e) {
  e.preventDefault();
  if (textGrocery.value !== "" && !editFlag) {
    // new grocery object
    let newGrosery = new Grocery(textGrocery.value);
    groceries.push(newGrosery);
    // new article on document
    const article = document.createElement("article");
    let dataID = document.createAttribute("data-id");
    dataID.value = newGrosery.id;
    article.setAttributeNode(dataID);
    article.classList.add("grocery-item");
    // add title and buttons to article
    article.innerHTML += `<p class="title">${newGrosery.title}</p>
    <div class="btn-container">
      <button type="button" class="edit-btn">
        <i class="fas fa-edit"></i>
      </button>
      <button type="button" class="delete-btn">
        <i class="fas fa-trash"></i>
      </button>
    </div>
  </article>`;
    // add click event to edit and delete buttons
    const deleteBtn = article.querySelector(".delete-btn");
    deleteBtn.addEventListener("click", deleteItem);
    const editBtn = article.querySelector(".edit-btn");
    editBtn.addEventListener("click", editItem);
    // add the article to the grocerylist on document
    groceryList.appendChild(article);
    // alert item added
    modifyAlert("alert-success", "Item added to the list");
    // show container
    groceryContainer.classList.add("show-container");
    // set default values
    setBackToDefault();
  } else if (textGrocery.value !== "" && editFlag) {
    // set the new title to the edited item
    editElement.innerHTML = textGrocery.value;
    // alert item edited
    modifyAlert("alert-success", "item edited");
    // set default values
    setBackToDefault();
  } else {
    // alert to enter a value for the item
    modifyAlert("alert-danger", "Please enter a value");
  }
}

function setBackToDefault() {
  textGrocery.value = "";
  editFlag = false;
  editID = "";
  submitBtn.textContent = "submit";
}

function deleteItem(e) {
  // get the article to delete
  // first parentElement is btns container target
  // second parentElement is the article target
  const element = e.currentTarget.parentElement.parentElement;
  // remove child from grocerylist
  groceryList.removeChild(element);
  // if grocerylist is empty removes the showcontainer class from grocerycontainer
  if (groceryList.children.length === 0) {
    groceryContainer.classList.remove("show-container");
  }
  // alert item removed
  modifyAlert("alert-danger", "item removed");
}

function editItem(e) {
  // get the article to edit
  const element = e.currentTarget.parentElement.parentElement;
  // get the <p> that get the title to edit
  editElement = e.currentTarget.parentElement.previousElementSibling;
  // put the actual value on the input grocery
  textGrocery.value = editElement.innerHTML;
  editFlag = true;
  editID = element.dataset.id;
  submitBtn.textContent = "edit";
}

function modifyAlert(action, message) {
  alertMessage.classList.add(action);
  alertMessage.textContent = message;
  setTimeout(() => {
    alertMessage.classList.remove(action);
    alertMessage.textContent = "";
  }, 1500);
}
