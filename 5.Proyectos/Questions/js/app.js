const questions = [
    {
        id: 1,
        title: "Aceptan todas las tarjetas de credito?",
        text: "Si, aceptamos todas las tarjetas de credito :D",
    },
    {
        id: 2,
        title: "Apoyan a los agricultores locales?",
        text: "Si, apoyamos a los agricultores locales comprando grandes cantidades de los productos que producen.",
    },
    {
        id: 3,
        title: "Utilizan ingredientes organicos?",
        text: "Si, utilizamos ingredientes organicos para todos nuestros productos.",
    },
];

const sectionCenter = document.querySelector(".section-center");
window.addEventListener("DOMContentLoaded", loadQuestions());

function loadQuestions () {
    sectionCenter.innerHTML = questions.map(question =>
        `  <article class="question" id="art${question.id}">
        <div class="question-title">
          <p>${question.title}</p>
          <button type="button" class="question-btn" id="btn${question.id}">
            <span class="plus-icon">
              <i class="far fa-plus-square"></i>
            </span>
            <span class="minus-icon">
              <i class="far fa-minus-square"></i>
            </span>
          </button>
        </div>
        <div class="question-text">
          <p>${question.text}</p>
        </div>
      </article>`).join("");
}

const showBtns = document.querySelectorAll(".question-btn");
showBtns.forEach(btn => {
    btn.addEventListener("click", (e) => {
        let question = e.currentTarget;
        let article = document.getElementById(`art${question.id[3]}`);
        if (article.className === "question") {
            article.className = "question show-text";
        } else {
            article.className = "question";
        }
    });
});