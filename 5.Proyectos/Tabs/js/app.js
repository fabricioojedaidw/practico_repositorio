const items = [
  {
    title: "history",
    text: "I'm baby wolf pickled schlitz try-hard normcore marfa man bunmumblecore vice pop-up XOXO lomo kombucha glossier bicyclerights. Umami kinfolk salvia jean shorts offal venmo. Knausgaardtilde try-hard, woke fixie banjo man bun. Small batch tumeric mustache tbh wayfarers 8-bit shaman chartreuse tacos. Viral direct trade hoodie ugh chambray, craft beer pork belly flannel tacos single-origin coffee art party migas plaid pop-up.",
  },
  {
    title: "vision",
    text: "Man bun PBR&B keytar copper mug prism, hell of helvetica. Synth crucifix offal deep v hella biodiesel. Church-key listicle polaroid put a bird on it chillwave palo santo enamel pin, tattooed meggings franzen la croix cray. Retro yr aesthetic four loko tbh helvetica air plant, neutra palo santo tofu mumblecore. Hoodie bushwick pour-over jean shorts chartreuse shabby chic. Roof party hammock master cleanse pop-up truffaut, bicycle rights skateboard affogato readymade sustainable deep v live-edge schlitz narwhal.",
  },
  {
    title: "goals",
    text: "Chambray authentic truffaut, kickstarter brunch taxidermy vape heirloom four dollar toast raclette shoreditch church-key. Poutine etsy tote bag, cred fingerstache leggings cornhole everyday carry blog gastropub. Brunch biodiesel sartorial mlkshk swag, mixtape hashtag marfa readymade direct trade man braid cold-pressed roof party. Small batch adaptogen coloring book heirloom. Letterpress food truck hammock literally hell of wolf beard adaptogen everyday carry. Dreamcatcher pitchfork yuccie, banh mi salvia venmo photo booth quinoa chicharrones.",
  },
];

const btnContainer = document.querySelector(".btn-container");
const content = document.querySelector(".about-content");

window.addEventListener(
  "DOMContentLoaded",
  loadBtns()
);

function loadBtns() {
  btnContainer.innerHTML = items
    .map(
      (item) =>
        `<button class="tab-btn" data-id="${item.title}">${item.title}</button>`
    )
    .join("");
}

const itemBtns = document.querySelectorAll(".tab-btn");
itemBtns.forEach((btn) => {
  btn.addEventListener("click", (e) => {
    let btn = e.currentTarget;
    let id = btn.dataset.id;
    btn.className = "tab-btn active";
    itemBtns.forEach(btn => {
      if ( !(btn.dataset.id === id) ) {
        btn.className = "tab-btn";
      }
    });
    loadItem(id);
  });
});


function loadItem(title) {
  let item = items.find((item) => item.title === title);
  content.innerHTML = `<div class="content active" id="${item.title}">
  <h4>${item.title}</h4>
  <p>${item.text}</p>
</div>`;
}
