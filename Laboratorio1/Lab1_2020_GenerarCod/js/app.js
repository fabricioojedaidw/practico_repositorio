// Elementos html
const inputCodigo = document.getElementById("idCodigo");
const pResultado = document.getElementById("idResultado");
const pCodigo = document.getElementById("idCodigoCompleto");
const btnGenerar = document.getElementById("idBtnGenerar");
btnGenerar.addEventListener("click", () => {
  let cod = formatearCod(inputCodigo.value);
  if (cod === "") {
    alert(`Ingrese el codigo del producto.`);
  } else {
    let codF = traducirCod(cod);
    let impar = sumaImpares(codF);
    let par = sumaPares(codF);
    par = par * 3;
    let resultado = impar + par;
    resultado = multiploDiez(resultado);
    resultado = calcularResFinal(resultado);
    let codFinal = codF + resultado;
    pResultado.innerHTML = `Resultado: D&iacute;gito verificador ${resultado}`;
    pCodigo.innerHTML = `C&oacute;digo de producto con d&iacute;gito verificador: ${codFinal}`;
  }
});

function formatearCod(cod) {
  return cod.toUpperCase().replace(/ /g, "");
}

// Paso 1
function traducirCod(cod) {
  const letras = [
    ["A", "B", "C", "D", "E", "F", "G", "H", "I"],
    ["J", "K", "L", "M", "N", "O", "P", "Q", "R"],
    ["S", "T", "U", "V", "W", "X", "Y", "Z"],
  ];
  const vocales = ["A", "E", "I", "O", "U"];
  let codF = "";
  for (let i = 0; i < cod.length; i++) {
    let c = cod[i];
    if (isNaN(c)) {
      let n = 0;
      if (vocales.includes(c)) {
        for (let j = 0; j < letras.length; j++) {
          if (letras[j].includes(c)) {
            n = j + 2 + letras[j].indexOf(c) + 1;
          }
        }
      }
      codF += n;
    } else {
      codF += c;
    }
  }
  return codF;
}
// Paso 2
function sumaImpares(cod) {
  let suma = 0;
  for (let i = 1; i <= cod.length; i++) {
    if (i > 11) {
      if (i % 2 === 0) {
        suma += Number(cod[i - 1]);
      }
    } else {
      if (i % 2 !== 0) {
        suma += Number(cod[i - 1]);
      }
    }
  }
  return suma;
}
// Paso 3
function sumaPares(cod) {
  let suma = 0;
  for (let i = 1; i <= cod.length; i++) {
    if (i <= 10) {
      if (i % 2 === 0) {
        suma += Number(cod[i - 1]);
      }
    }
  }
  return suma;
}

// Paso 5
function multiploDiez(n) {
  if (n % 10 === 0) {
    return n;
  }
  while (!(n % 10 === 0)) {
    n++;
  }
  return n;
}

function calcularResFinal(n) {
  let resultado = String(n);
  let resFinal;
  while (resultado.length > 1) {
    resFinal = 0;
    for (let i = 0; i < resultado.length; i++) {
      resFinal += Number(resultado[i]);
    }
    resultado = String(resFinal);
  }
  return resFinal;
}
