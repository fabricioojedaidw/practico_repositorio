const letras = [
  ["A", "J", "S"],
  ["B", "K", "T"],
  ["C", "L", "U"],
  ["D", "M", "V"],
  ["E", "N", "W"],
  ["F", "O", "X"],
  ["G", "P", "Y"],
  ["H", "Q", "Z"],
  ["I", "R"],
];

const btnDestino = document.getElementById("idBtnDestino");
btnDestino.addEventListener("click", () => {
  let nombreIngresado = document.getElementById("idNombre").value;
  if (nombreIngresado === "" || !isNaN(nombreIngresado)) {
    alert(`Ingrese su nombre`);
  } else {
    nombreIngresado = formateoNombre(nombreIngresado);
    let nroDestino = calcularNroDestino(nombreIngresado);
    const inputDestino = document.getElementById("idNroDestino");
    inputDestino.value = nroDestino;
  }
});

function calcularNroDestino(nombre) {
  let resultado = 0;
  let c = "";
  // Calcula el resultado del nombre completo.
  for (let i = 0; i < nombre.length; i++) {
    c = nombre[i];
    for (let j = 0; j < letras.length; j++) {
      if (letras[j].includes(c)) {
        resultado += j + 1;
      }
    }
  }
  let resFinal = calcularResFinal(resultado);
  return resFinal;
}

const inputNombre = document.getElementById("idNombre");
inputNombre.addEventListener("keypress", () => {
  let contenido = inputNombre.value;
  contenido = contenido.replace(/[0-9]/g, "");
  inputNombre.value = contenido;
});

const btnPersonalidad = document.getElementById("idBtnPersonalidad");
btnPersonalidad.addEventListener("click", () => {
  const fechaIngresada = inputFecha.value;
  if (fechaIngresada === "") {
    alert("Ingrese su fecha de nacimiento.");
  } else {
    const dia = String(fechaIngresada).split("-")[2];
    let nro = calcularResFinal(dia);
    const inputPersonalidad = document.getElementById("idNroPersonalidad");
    inputPersonalidad.value = nro;
  }
});

function calcularResFinal(n) {
  let resultado = String(n);
  let resFinal;
  while (resultado.length > 1) {
    resFinal = 0;
    for (let i = 0; i < resultado.length; i++) {
      resFinal += Number(resultado[i]);
    }
    resultado = String(resFinal);
  }
  return resFinal;
}

const inputFecha = document.getElementById("idFechaNacimiento");
inputFecha.setAttribute("max", obtenerFechaActual());

function obtenerFechaActual() {
  var fecha = new Date();
  var dia = fecha.getDay();
  var mes = fecha.getMonth();
  var anio = fecha.getFullYear();
  if (dia < 10) {
    dia = `0${dia + 1}`;
  }
  if (mes < 10) {
    mes = `0${mes + 1}`;
  }
  return `${anio}-${mes}-${dia}`;
}

function formateoNombre(nombre) {
  nombre = nombre.toUpperCase();
  nombre = nombre.replace(" ", "");
  nombre = nombre.replace(/[0-9]/g, "");
  return nombre;
}