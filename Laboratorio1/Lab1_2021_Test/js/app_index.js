const resultadosTest = [];
var nombre, edad, direccion, puesto, puntaje;

const btnEvaluar = document.getElementById("btnEvaluar");
btnEvaluar.addEventListener("click", () => {
  if (camposVacios()) {
    alert("Ingrese los datos personales solicitados.");
  } else {
    mostrarResultado();
  }
});

const btnBorrar = document.getElementById("btnBorrar");
btnBorrar.addEventListener("click", () => {
  location.reload();
});

function camposVacios() {
  const tabla = document.getElementById("idDatosPersonales");
  const inputs = tabla.getElementsByTagName("input");
  let vacio = false;
  let bandera = true;
  for (let i = 0; i < inputs.length; i++) {
    if (bandera) {
      if (inputs[i].value === "") {
        vacio = true;
        bandera = false;
      }
    }
  }
  return vacio;
}

function borrarResultado() {
  // Borra el anterior resultado del test.
  const padre = document.getElementById("idContenido");
  let anteriorResultado = document.getElementById("idResultado");
  if (anteriorResultado) {
    padre.removeChild(anteriorResultado);
  }
}

function mostrarResultado() {
  const padre = document.getElementById("idContenido");
  // Borra el resultado del utlimo test hecho.
  borrarResultado();
  let resultado = calcularPuntaje();
  puntaje = resultado;
  // Crea el articulo que contendra el resultado y setea el id.
  const article = document.createElement("article");
  article.setAttribute("id", "idResultado");
  // Creo el titulo del resultado.
  const h3 = document.createElement("h3");
  h3.innerHTML = "Resultado del Test";
  article.appendChild(h3);
  // Recorre el arreglo con los resultados de cada pregunta y crea un parrafo para cada resultado.
  for (let i = 0; i < resultadosTest.length; i++) {
    let p = document.createElement("p");
    p.innerHTML = `Pregunta ${i + 1}: ${resultadosTest[i]} puntos.`;
    article.appendChild(p);
  }
  // Crea y setea el parrafo con el puntado total del test.
  let pPuntaje = document.createElement("p");
  pPuntaje.innerHTML = `Puntaje total del test: ${resultado}`;
  article.appendChild(pPuntaje);
  // Crea y setea el parrafo que contiene para que puesto califica.
  let calificacion = document.createElement("p");
  calificacion.innerHTML = evaluarCondicion(resultado);
  article.appendChild(calificacion);
  // Crea el boton para enviar el formulario.
  let btnEnviar = crearBoton();
  article.appendChild(btnEnviar);
  padre.appendChild(article);
  cargarDatos();
  resultadosTest.length = 0;
}

function crearBoton () {
  let btnEnviar = document.createElement("input");
  btnEnviar.setAttribute("type", "submit");
  btnEnviar.setAttribute("id", "idBtnEnviar");
  btnEnviar.setAttribute("name", "btnEnviar");
  btnEnviar.setAttribute("value", "Enviar test");
  btnEnviar.addEventListener("click", () => {
    const form = document.getElementById("idFormResultados");
    form.submit();
  });
  return btnEnviar;
}

function cargarDatos () {
  const apellidoNombre = document.getElementById("idApellidoNombre");
  const inputNombreIngresado = document.getElementById("idNombreIngresado");
  inputNombreIngresado.value = apellidoNombre.value;
  const edad = document.getElementById("idEdad");
  const inputEdadIngresado = document.getElementById("idEdadIngresado");
  inputEdadIngresado.value = edad.value;
  const direccion = document.getElementById("idDomicilio");
  const inputDireccionIngresado = document.getElementById("idDireccionIngresado");
  inputDireccionIngresado.value = direccion.value;
  const inputPuesto = document.getElementById("idPuestoObtenido");
  inputPuesto.value = puesto;
  const inputPuntaje = document.getElementById("idPuntajeObtenido");
  inputPuntaje.value = puntaje;
}

function evaluarCondicion(puntaje) {
  // Evalua la condicion segun el puntaje obtenido.
  let mensaje = `Tu puntaje califica para: `;
  if (puntaje >= 8) {
    puesto = `Cocinero`;
    return (mensaje += `${puesto}.`);
  }
  if (puntaje >= 6 && puntaje < 8) {
    puesto = `Ayudante de cocina`;
    return (mensaje += `${puesto}.`);
  }
  if (puntaje < 6) {
    puesto = `No aprueba`;
    return `No aprueba el test, por lo tanto no califica para ningún puesto.`;
  }
}

function suma(idPregunta, puntaje) {
  // Retorna la suma de cada respuesta segun el idPregunta y el arreglo con los valores.
  const p1 = document.getElementById(idPregunta);
  let inputs = p1.getElementsByTagName("input");
  let res = 0;
  for (let i = 0; i < inputs.length; i++) {
    if (inputs[i].checked) {
      res += puntaje[i];
    }
  }
  return res;
}
function calcularPuntaje() {
  // Arreglos con los puntajes de cada pregunta y el porcentaje correspondiente a la pregunta.
  const puntajeP1 = [3, 2, 2, 0, 1, 2, -1, 0.35];
  const puntajeP2 = [0, 10, 0.1];
  const puntajeP3 = [10, 0, 0.08];
  const puntajeP4 = [2, 0, 9, 0.22];
  const puntajeP5 = [0, 0, 10, 0.25];
  // Asigna la suma de cada pregunta y asigna el resultado al arreglo de resultados.
  let p1 = suma("p1", puntajeP1);
  resultadosTest.push(p1);
  let p2 = suma("p2", puntajeP2);
  resultadosTest.push(p2);
  let p3 = suma("p3", puntajeP3);
  resultadosTest.push(p3);
  let p4 = suma("p4", puntajeP4);
  resultadosTest.push(p4);
  let p5 = suma("p5", puntajeP5);
  resultadosTest.push(p5);
  // Calcula el puntaje total y lo retorna.
  let puntajeTotal =
  p1 * puntajeP1[puntajeP1.length - 1] +
  p2 * puntajeP2[puntajeP2.length - 1] +
  p3 * puntajeP3[puntajeP3.length - 1] +
  p4 * puntajeP4[puntajeP4.length - 1] +
  p5 * puntajeP5[puntajeP5.length - 1];
  return puntajeTotal;
}