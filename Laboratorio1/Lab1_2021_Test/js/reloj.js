const reloj = document.getElementById("idFechaHora");
reloj.innerHTML = obtenerFecha();

function obtenerFecha () {
    let fecha = new Date();
    let dia = fecha.getDay();
    let mes = fecha.getMonth();
    let anio = fecha.getFullYear();
    let hora = fecha.getHours();
    let min = fecha.getMinutes();
    let seg = fecha.getSeconds();
    return `${dia+1}/${mes+1}/${anio} ${hora}:${min}:${seg}`;
}

setInterval(() => {
    reloj.innerHTML = obtenerFecha();
}, 1000);