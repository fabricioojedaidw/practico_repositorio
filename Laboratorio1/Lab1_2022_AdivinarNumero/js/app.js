// Generar un numero random del 1 al 100
// Contador de intentos (max 10)
// btnAdivinar = Comparar con el nro random / Indicar nro de intento / Dar indicio cerca/lejos
// btnRendirse = Indicar el nro random y no permitir jugar
// btnJugar = Resetea todo el juego.

// Variables y constantes del juego.
var nroIntentos = 1;
const MAXINTENTOS = 10;
const MIN = 1;
const MAX = 100;
var nroRandom = generarNroRandom();

// Elementos html.
const pIntento = document.getElementById("idIntento");
const pResultado = document.getElementById("idResultado");
const inputNro = document.getElementById("idNumeroIngresado");

inputNro.setAttribute("min", MIN);
inputNro.setAttribute("max", MAX);

const btnAdivinar = document.getElementById("idBtnAdivinar");
btnAdivinar.addEventListener("click", () => {
  let nroIngresado = Number(inputNro.value);
  if (nroIntentos === MAXINTENTOS) {
    incrementarIntentos();
    mostrarResultado();
  } else {
    if (nroIngresado < 1 || nroIngresado > 100) {
      alert(`Ingrese un numero del 1 al 100.`);
      inputNro.value = "";
    } else {
      incrementarIntentos();
      pResultado.innerText = compararNro();
    }
  }
});

const btnRendirse = document.getElementById("idBtnRendirse");
btnRendirse.addEventListener("click", () => {
  mostrarResultado();
});

const btnJugar = document.getElementById("idBtnJugar");
btnJugar.addEventListener("click", () => {
  deshabilitarInputs(false);
  // Resetear todo;
  nroRandom = generarNroRandom();
  nroIntentos = 1;
  pIntento.innerHTML = "";
  pResultado.innerHTML = "";
  inputNro.value = "";
});

// Funciones.

function compararNro() {
  let nroIngresado = Number(inputNro.value);
  let msj = `Resultado: Tu numero: ${nroIngresado} `;
  if (nroIngresado === nroRandom) {
    deshabilitarInputs(true);
    return (msj += `es el correcto. Felicitaciones adivinaste el numero.`);
  }
  if (nroIngresado > nroRandom) {
    return (msj += `es mayor.`);
  }
  if (nroIngresado < nroRandom) {
    return (msj += `es menor.`);
  }
}

function mostrarResultado() {
  deshabilitarInputs(true);
  pResultado.innerHTML = `Resultado: El numero es el ${nroRandom}. Intentalo nuevamente.`;
}

function incrementarIntentos() {
  let msj = `Nro. de intento: ${nroIntentos++}`;
  pIntento.innerHTML = msj;
}

function deshabilitarInputs(val) {
  inputNro.disabled = val;
  btnAdivinar.disabled = val;
  btnRendirse.disabled = val;
}

function generarNroRandom() {
  return Math.floor(Math.random() * (MAX - MIN)) + MIN;
}
