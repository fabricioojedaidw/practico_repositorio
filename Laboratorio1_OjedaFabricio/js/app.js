// 4 filas 4 valores
// 20 intentos disponibles
// si acierta no se pueden seleccionar nuevamente
// 2 cartas por tirada
/***
 * Finalizar
 * se llego a 20 intentos
 * se descubren las 10 cartas antes de llegar a 20 intentos
 * el jugador se rinde
 */

/**
 * Cuando finaliza el juego
 * 10 aciertos = excelente memoria
 * 8 y 9 acieros = muy buena memoria
 * 6 y 7 aciertos = buena memoria puedes mejorar
 * 6 = mala memoria debes practicar mas
 * abandona = que pena abandonaste
 */

var cantClick = 0;
const MAX_CLICK = 2;
var cantIntentos = 0;
const MAX_INTENTOS = 20;
var cantAciertos = 0;
const MAX_ACIERTOS = 10;
var cantPartidas = 0;
const cartasSeleccionadas = [];
const botonesApretados = [];
const valores = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

const pIntentos = document.getElementById("idIntentos");
const pAciertos = document.getElementById("idAciertos");
const pPartidas = document.getElementById("idPartida");
const pResultado = document.getElementById("idResultado");

const cartas = document.getElementsByClassName("carta");
for (const carta of cartas) {
  carta.addEventListener("click", () => {
    cantClick++;
    if (cantIntentos !== MAX_INTENTOS) {
      if (cantClick !== MAX_CLICK) {
        let valor = carta.value;
        cartasSeleccionadas.push(valor);
        botonesApretados.push(carta);
        carta.disabled = true;
        carta.innerHTML = valor;
      } else {
        let valor = carta.value;
        cartasSeleccionadas.push(valor);
        botonesApretados.push(carta);
        carta.innerHTML = valor;
        if (cartasSeleccionadas[0] === cartasSeleccionadas[1]) {
          botonesApretados[0].disabled = true;
          botonesApretados[1].disabled = true;
          vaciarSelecciones();
          aumentarAciertos();
          checkVictoria();
        } else {
          setTimeout(() => {
            botonesApretados[0].innerHTML = 0;
            botonesApretados[1].innerHTML = 0;
            botonesApretados[0].disabled = false;
            botonesApretados[1].disabled = false;
            vaciarSelecciones();
          }, 500);
        }
        aumentarIntentos();
      }
    } else {
        alert("Alcanzaste el maximo de intentos (20)");
      deshabilitarTablero(true);
      pResultado.innerHTML = darResultado();
    }
  });
}

const btnFinalizar = document.getElementById("btnFinalizar");
btnFinalizar.addEventListener("click", () => {
  deshabilitarTablero(true);
  pResultado.innerHTML = darResultado();
  btnFinalizar.disabled = true;
});

const btnIniciar = document.getElementById("btnIniciar");
btnIniciar.addEventListener("click", () => {
  incrementarPartida();
  restablecerJuego();
  btnFinalizar.disabled = false;
});

function checkVictoria() {
  if (cantAciertos === MAX_ACIERTOS) {
    pResultado.innerHTML = `EXCELENTE MEMORIA!!!!`;
    btnFinalizar.disabled = true;
    deshabilitarTablero(true);
    return true;
  }
}
function darResultado() {
  if (cantAciertos >= 8 && cantAciertos <= 9) {
    return `MUY BUENA MEMORIA!!!!`;
  }
  if (cantAciertos >= 6 && cantAciertos <= 7) {
    return `BUENA MEMORIA!!!! PUEDE MEJORAR!!!!`;
  }
  if (cantAciertos >= 1 && cantAciertos < 6) {
    return `Mala memoria, debes practicar mas!!!!`;
  }
  if (cantAciertos === 0) {
    return `No has acertado ni una!!!!`;
  }
}

function deshabilitarTablero(val) {
  for (let i = 0; i < cartas.length; i++) {
    cartas[i].disabled = val;
  }
}

function vaciarSelecciones() {
  cantClick = 0;
  cartasSeleccionadas.length = 0;
  botonesApretados.length = 0;
}

function aumentarIntentos() {
  cantIntentos++;
  pIntentos.innerHTML = `Intentos: ${cantIntentos}`;
}

function aumentarAciertos() {
  cantAciertos++;
  pAciertos.innerHTML = `Aciertos: ${cantAciertos}`;
}

function mezclarCartas() {
  let j = 0;
  barajar();
  for (let i = 0; i < cartas.length; i++) {
    if (i === valores.length) {
      barajar();
      j = 0;
    }
    cartas[i].setAttribute("value", valores[j]);
    j++;
  }
}
function barajar() {
  valores.sort(() => Math.random() - 0.5);
}

function restablecerJuego() {
  for (let i = 0; i < cartas.length; i++) {
    cartas[i].innerHTML = 0;
  }
  mezclarCartas();
  deshabilitarTablero(false);
  vaciarSelecciones();
  cantAciertos = 0;
  cantClick = 0;
  cantIntentos = 0;
  pIntentos.innerHTML = `Intentos:`;
  pAciertos.innerHTML = `Aciertos:`;
  pResultado.innerHTML = ``;
}

function incrementarPartida() {
  cantPartidas++;
  pPartidas.innerHTML = `Partida Nro ${cantPartidas}`;
}

incrementarPartida();
mezclarCartas();
