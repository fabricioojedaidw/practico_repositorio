function esMayorEdad() {
  do {
    var edad = prompt("Ingrese su edad");
    if (edad >= 18) {
      mostrarContenido();
    } else {
      alert("Debe ser mayor de edad para ingresar a la página.");
    }
  } while (edad < 18);
}

function mostrarHora() {
  const hora = document.getElementById("idHora");
  let fecha = new Date();
  let h = fecha.getHours();
  let min = fecha.getMinutes();
  let seg = fecha.getSeconds();
  hora.innerHTML = `Hora: ${h}:${min}:${seg}`;
}

function mostrarContenido() {
  document.write(`
    <header>
        <h1>Ejercicio Nro. 1</h1>
    </header>
    <section>
        <article>
            <p id="idHora"></p>
        </article>
        <article>
            <button onclick="mostrarMensaje()">Mostrar Mensaje</button>
    </section>`);
  mostrarHora();
  setInterval(() => {
    mostrarHora();
  }, 1000);
}

function mostrarMensaje() {
  alert("Nueva ventana.");
}
