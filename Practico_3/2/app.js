/**
 * 2. Definir una función que muestre información sobre una cadena de texto que ingresa el usuario.
 * Informar si la cadena está formada sólo por mayúsculas, sólo por minúsculas o por una mezcla de
 * ambas.
 */

const btnEvaluar = document.getElementById("id1");
btnEvaluar.addEventListener("click", () => {
  const resultado = document.getElementById("resultado");
  resultado.innerHTML = `Resultado: ${evaluarTexto()}`;
});

function evaluarTexto() {
  const texto = document.getElementById("idTexto").value.trim();
  if (texto.length > 0) {
    //Variables para el indice de la primera mayuscula y minuscula.
    let mayus, minus;
    //Expresiones regulares que contienen a...z y A...Z
    let minusculas = new RegExp("[a-z]");
    let mayusculas = new RegExp("[A-Z]");
    let p = "";
    //Busca si el texto ingresado contiene almenos una mayusculas y un minuscula.
    mayus = texto.search(mayusculas);
    minus = texto.search(minusculas);
    //Si el resultado de la búsqueda de mayúsculas es mayor a -1 y minúsculas es mayor a -1, significa que encontró coincidencias en ambos.
    if (mayus > -1 && minus > -1) {
      return `Texto formado por combinacion de may&uacute;sculas y min&uacute;sculas.`;
    }
    //Si el resultado de búsqueda de mayúsculas retorno -1 significa que no encontro mayúsculas en el texto y este es solo formado por minúsculas.
    if (mayus === -1) {
      return `Texto formado solo por min&uacute;sculas.`;
    }
    //Si el resultado de búsqueda de minúsculas retorno -1 significa que no encontro minúsculas en el texto y este es solo formado por mayúsculas.
    if (minus === -1) {
      return `Texto formado solo por may&uacute;sculas`;
    }
  } else {
      return `Ingrese un texto.`;
  }
}

/** MANERA MAS EFICIENTE UTILIZANDO EXPRESIONES REGULARES
 function checkType(words) {
  words = String(words).trim();
  const regxs = {
    "lower": /^[a-z0-9 ]+$/,
    "upper": /^[A-Z0-9 ]+$/,
    "upperLower": /^[A-Za-z0-9 ]+$/
  }
 
  if (regxs.lower.test(words)) return '0';
  if (regxs.upper.test(words)) return '1';
  if (regxs.upperLower.test(words)) return '2';
  return -1;
}
 */