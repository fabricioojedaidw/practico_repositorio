function solicitarDni() {
  let dni = "";
  let valido = false;
  //Solicitud de DNI.
  do {
    dni = prompt("Ingrese su DNI.");
    valido = verificarDni(dni);
    if (!valido) {
        alert("DNI no válido.");
    }
  } while (!valido);
  //Solicitud de letra.
  valido = false;
  let letra = prompt("Ingrese la letra asociada a su DNI.").trim()[0];
  //Validación de letra.
  valido = verificarLetra(dni, letra);
  //Mensaje correspondiente.
  valido ? alert("Bienvenido.") : alert("Letra incorrecta.");
}

function verificarDni(dni) {
  //Verifíca si el dni ingresado es de 8 dígitos.
  return parseInt(dni) >= 10000000 && parseInt(dni) <= 99999999;
}
function verificarLetra(dni, letra) {
  const letras = [
    "T",
    "R",
    "W",
    "A",
    "G",
    "M",
    "Y",
    "F",
    "P",
    "D",
    "X",
    "B",
    "N",
    "J",
    "Z",
    "S",
    "Q",
    "V",
    "H",
    "L",
    "C",
    "K",
    "E",
    "T",
  ];
  letras.push('2');
  //Verifíca si es un número
  if (!isNaN(letra)) {
    return false;
  }
  //Retorna la comparación entre la letra ingresada y la letra correspondiente. 
  return letra.toUpperCase() === letras[parseInt(dni) % 23];
}
