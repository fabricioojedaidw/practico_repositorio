//Desactivar click derecho en la página.
// window.oncontextmenu = (e) => {
//   e.preventDefault();
// };
//Arreglo con la url de cada imagen.
const urls = [
  { url: "img/imagen1.jpg" },
  { url: "img/imagen2.jpg" },
  { url: "img/imagen3.png" },
  { url: "img/imagen4.jpg" },
  { url: "img/imagen5.jpg" },
  { url: "img/imagen6.jpg" },
  { url: "img/imagen7.jpg" },
  { url: "img/imagen8.jpg" },
  { url: "img/imagen9.jpg" },
  { url: "img/imagen10.jpg" },
  { url: "img/imagen11.jpg" },
  { url: "img/imagen12.jpg" },
];
//Variable para controlar la imágen que se está mostrando actualmente.
var actual = 0;
//Arreglo con el código html de cada imagen con su respectivo arreglo.
let i = 0;
const imagenes = urls.map((url) => {
  ++i;
  return `<article>
        <img id="img1" src="${url.url}" alt="img${i}" title="img${i}" class="img">
    </article>`;
});
//Agregar funcionalidad al botón siguiente.
const btnSiguiente = document.getElementById("btnSiguiente");
btnSiguiente.addEventListener("click", () => {
  //Se incrementa la variable actual y se llama al método cargarImagen()
  siguienteImagen();
});
//Agregar funcionalidad al botón anterior.
const btnAnterior = document.getElementById("btnAnterior");
btnAnterior.addEventListener("click", () => {
  //Se decrementa la variable actual y se llama al método cargarImagen()
  anteriorImagen();
});

function cargarImagen(ind) {
  //Se obtiene el div contenedor.
  const contenedor = document.getElementById("cont-img");
  //Se obtiene el elemento con el id img1
  const oldImg = document.getElementById("img1");
  //Si existe oldImg se busca el padre del nodo y se borra este nodo hijo (oldImg)
  if (oldImg) {
    const padre = oldImg.parentNode;
    padre.removeChild(oldImg);
  }
  //Se asigna la imágen actual al div contenedor.
  contenedor.innerHTML = `${imagenes[ind]}`;
}

function siguienteImagen () {
  if (actual === urls.length -1) {
      actual = 0;
      cargarImagen(actual);
  } else {
    actual++;
    cargarImagen(actual);
  }
}

function anteriorImagen () {
  if (actual === 0) {
      actual = urls.length-1;
      cargarImagen(actual);
  } else {
    actual--;
    cargarImagen(actual);
  }
}