var expresion = "";
const cuenta = document.getElementById("idCuenta");
const btns = document.querySelectorAll(".btn");
btns.forEach((btn) => {
  btn.addEventListener("click", () => {
    cargarValor(btn.value);
  });
});

function cargarValor(val) {
  //variable auxiliar que contiene la operación ingresada.
  let contenido = String(cuenta.value);
  //Asigna el último valor ingresado a la variable auxiliar.
  contenido += val;
  //Varbiable con el anteúltimo valor ingresado.
  let ant = contenido[contenido.length - 2];
  //Evalua si el nuevo valor es algún operador.
  switch (val) {
    case "/":
    case "*":
    case "-":
    case "+":
    case "r":
    case "%":
    case "1/":
    case ".":
      switch (ant) {
        //Evalua si el anteúltimo valor es algún operador.
        case "/":
        case "*":
        case "-":
        case "+":
        case "r":
        case "%":
        case "1/":
        case ".":
          //Si es algún operador lo borra y asigna el nuevo operador ingresado.
          contenido = contenido.substring(0, contenido.length - 2);
          cuenta.value = contenido + val;
          break;
        default:
          //Si es un número lo asigna a la operación.
          cuenta.value += `${val}`;
          break;
      }
      break;
    default:
      //Si es un número lo asigna a la operación.
      cuenta.value += `${val}`;
      break;
  }
}

const btnCE = document.getElementById("CE");
btnCE.addEventListener("click", () => {
  //Borra todo el contenido.
  cuenta.value = "";
});

const btnC = document.getElementById("C");
btnC.addEventListener("click", () => {
  //Borra el último valor ingresado.
  let contenido = String(cuenta.value);
  cuenta.value = contenido.substring(0, contenido.length - 1);
});

const btnIgual = document.getElementById("btnIgual");
btnIgual.addEventListener("click", () => {
  //Quita el = de la operacion.
  let operacion = String(cuenta.value).substring(0, cuenta.value.length - 1);
  let resultado;
  try {
    //Resuelve la operación si no esta vacía.
    if (operacion !== "") {
      resultado = eval(`${operacion}`);
      cuenta.value += resultado;
    } else {
      cuenta.value = `Ingrese una operación.`;
    }
  } catch (error) {
    //Captura la excepción y muestra el mensaje.
    cuenta.value = `Operación no valida.`;
  }
});
