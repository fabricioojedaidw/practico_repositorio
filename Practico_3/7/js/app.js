// Cambiar el nombre a las funciones para generar los radiobuttons/checkbox para que sean mas genericas. Funciona con los dos tipos de elementos.
// Agregar estilos CSS para todos los inputs y formularios.
const contenido = [];
const botonesGenerados = [];
var idRadioBtn = 0;
var idBtnBorrar = 0;
var idCheckBox = 0;
const opcionesTipos = document.getElementById("idTipos");
opcionesTipos.addEventListener("change", () => {
  const opciones = document.getElementById("idTipos");
  let index = opciones.selectedIndex;
  let tipo = opciones[index].value;
  borrarCamposOpciones();
  if (tipo === "radio" || tipo === "checkbox") {
    generarOpcionRadioButtons();
    const cantidad = document.getElementById("idCantidad");
    cantidad.addEventListener("change", () => {
      let cant = cantidad.value;
      generarCantidadInputRadio(cant);
    });
  }
  if (tipo === "button") {
    generarOpcionesBoton();
  }
});
const btnGenerar = document.getElementById("idGenerar");
btnGenerar.addEventListener("click", () => {
  const titulo = document.getElementById("idTitulo").value.trim();
  const opciones = document.getElementById("idTipos");
  let index = opciones.selectedIndex;
  let tipo = opciones[index].value;
  if (!camposVacios(titulo, tipo)) {
    if (!elementoRepetido(titulo)) {
      if (tipo === "radio" || tipo === "checkbox") {
        agregarRadioBtns(titulo, tipo);
      } else if (tipo === "button") {
        agregarBoton(titulo);
      } else {
        agregarInput(titulo, tipo);
      }
    } else {
      alert(`El formulario ya contiene el campo ${titulo}`);
    }
  } else {
    alert(`Llene los dos campos.`);
  }
});
function formatearTitulo(titulo) {
  // Formatea el texto haciendo mayuscula la primer letra.
  return titulo[0].toUpperCase() + titulo.substring(1, titulo.length);
}
function agregarInput(titulo, tipo) {
  // Agrega al formulario el campo con su titulo y su input.
  const padre = document.getElementById("idContenidoForm");
  // Creo nueva fila para la tabla que contiene los elementos del formulario.
  const fila = document.createElement("tr");
  // Creo el boton para borrar el input.
  const btnDel = generarBtnDel(titulo);
  fila.appendChild(btnDel);
  // Creo una celda para el nuevo lbl.
  const celdaLbl = document.createElement("td");
  fila.appendChild(celdaLbl);
  // Creo nuevo lbl para el nuevo input. Luego seteo sus atributos.
  const nuevoLbl = document.createElement("label");
  let tituloF = titulo.replace(" ", "_");
  let id = `id${tituloF}`;
  nuevoLbl.setAttribute("for", id);
  nuevoLbl.innerHTML = `${formatearTitulo(titulo)}:`;
  // Agrego el lbl a su celda.
  celdaLbl.appendChild(nuevoLbl);
  // Creo una celda para el nuevo input
  const celdaInput = document.createElement("td");
  // Creo el nuevo input y seteo sus atributos.
  const nuevoInput = document.createElement("input");
  nuevoInput.setAttribute("type", tipo);
  nuevoInput.setAttribute("name", tituloF);
  nuevoInput.setAttribute("id", id);
  // Agrego el nuevo input a su celda.
  celdaInput.appendChild(nuevoInput);
  // Agrego el lbl e input a la celda.
  fila.appendChild(celdaLbl);
  fila.appendChild(celdaInput);
  // Agrego la fila que contiene la celda que contiene el lvl e input a la tabla.
  padre.appendChild(fila);
  // Se agrega al arreglo contenido el titulo del input agregado.
  contenido.push(titulo.toUpperCase());
  // Reseteo los valores de los inputs.
  resetearValores();
}

function borrarHijo(e, titulo) {
  // Funcion para borrar el elemento del formulario.
  const hijo = e.currentTarget.parentNode; // Obtiene el hijo que ocaciono el evento "click".
  const contenidoForm = document.getElementById("idContenidoForm"); // Obtiene el formulario.
  contenidoForm.removeChild(hijo); // Remueve el hijo que ocaciono el evento.
  let index = contenido.indexOf(titulo.toUpperCase()); // Obtiene el index del titulo del campo que es elminiado.
  contenido.splice(index, 1); // Borra el titulo eliminado del formulario.
}

function generarBtnDel(titulo) {
  // Genera un boton para eliminar cada campo generado.
  // Crea el elemento input, setea los atributos id/type/value/title.
  const btnDel = document.createElement("input");
  btnDel.setAttribute("id", `btnDel${idBtnBorrar++}`);
  btnDel.setAttribute("type", "button");
  btnDel.setAttribute("value", "x");
  btnDel.setAttribute("title", `Borrar este campo`);
  // Agrega el evento para borrar el elemento al presionar el boton.
  btnDel.addEventListener("click", (e) => {
    borrarHijo(e, titulo);
  });
  return btnDel;
}

function elementoRepetido(titulo) {
  // Comprueba si el titulo del campo que se quiere agregar ya esta en el formulario generado.
  return contenido.includes(titulo.toUpperCase());
}

function camposVacios(titulo, tipo) {
  // Comprueba si algun campo esta vacio.
  return titulo === "" || tipo === "--";
}

function resetearValores() {
  // Resetea los valores del titulo y el selector de opciones.
  const inputTitulo = document.getElementById("idTitulo");
  const inputOpciones = document.getElementById("idTipos");
  inputTitulo.value = "";
  inputOpciones.selectedIndex = 0;
}

function generarOpcionRadioButtons() {
  // Genera el campo para seleccionar la cantidad de radio buttons.
  // Obtengo el formulario original de selecciones.
  const padre = document.getElementById("idSeleccionesForm");
  // Creo una fila y seteo el id.
  const fila = document.createElement("tr");
  fila.setAttribute("id", "idFilaCantidad");
  // Creo una celda y se la agrego a la fila creada anteriormente.
  const celdaLbl = document.createElement("td");
  fila.appendChild(celdaLbl);
  // Creo nuevo label para la cantidad de radio buttons, se agrega el titulo y se lo agrega a su celda.
  const nuevoLbl = document.createElement("label");
  nuevoLbl.innerHTML = "Cantidad:";
  celdaLbl.appendChild(nuevoLbl);
  // Creo una celda para el input de cantidad.
  const celdaInput = document.createElement("td");
  // Creo el input para la cantidad, seteo los atributos id/type/max/min y luego se lo agrega a su celda.
  const cantidad = document.createElement("input");
  cantidad.setAttribute("id", "idCantidad");
  cantidad.setAttribute("type", "number");
  cantidad.setAttribute("max", "5");
  cantidad.setAttribute("min", "1");
  celdaInput.appendChild(cantidad);
  // Se agrega a la fila la celda que contiene el lbl y la celda que contiene el input
  fila.appendChild(celdaLbl);
  fila.appendChild(celdaInput);
  // Se agrega la fila al padre (tabla formulario).
  padre.appendChild(fila);
}

function generarCantidadInputRadio(cant) {
  // Genera los inputs para agregar el valor de cada radio button.
  // Obtengo el formulario padre.
  const padre = document.getElementById("idSeleccionesForm");
  // Obtengo la vieja fila y si existe la remuevo.
  const oldFila = document.getElementById("idCantidadRadio");
  if (oldFila) {
    padre.removeChild(oldFila);
  }
  // Creo la fila que contendra el label y los inputs segun la cantidad seleccionada.
  const fila = document.createElement("tr");
  fila.setAttribute("id", "idCantidadRadio");
  // Creo la celda que contendra el label y la agrego a la fila.
  const celdaLbl = document.createElement("td");
  fila.appendChild(celdaLbl);
  // Creo el label para el titulo y lo agrego a la celda.
  const lbl = document.createElement("label");
  lbl.innerHTML = `Valores: `;
  celdaLbl.appendChild(lbl);
  // Creo la celda que contendra la cantidad de inputs para cargar los valores de los radio buttons.
  const celdaInputs = document.createElement("td");
  // Genero los inputs y los agrego a la celdaInputs.
  for (let i = 1; i <= cant; i++) {
    let unInput = document.createElement("input");
    unInput.setAttribute("id", `idValorRadioBtn${i}`);
    unInput.setAttribute("type", "text");
    celdaInputs.appendChild(unInput);
  }
  // Agrego la celda de inputs a la fila.
  fila.appendChild(celdaInputs);
  // Agrego la fila al formulario.
  padre.appendChild(fila);
}

function agregarRadioBtns(titulo, tipo) {
  // Agrega los radio buttons al formulario.
  const padre = document.getElementById("idContenidoForm");
  const cantidad = document.getElementById("idCantidad").value; // Obtiene la cantidad de radio buttons que fue seleccionada.
  if (!(cantidad === "")) {
    // Si el input no esta vacio.
    const inputs = []; // Arreglo que contendra los inputs.
    let vacio = false; // Variable booleana para controlar que ningun input este vacio.
    let flag = true;
    for (let i = 1; i <= cantidad; i++) {
      // Bucle que obtiene los inputs que contienen el valor/titulo de cada radio button.
      let unInput = document.getElementById(`idValorRadioBtn${i}`);
      if (flag) {
        if (unInput.value === "") {
          vacio = true;
          flag = false;
        }
      }
      inputs.push(unInput); // Agrega el input al arreglo.
    }
    if (!vacio) {
      // Si ningun input esta vacio.
      const fila = document.createElement("tr"); // Creo una fila.
      const btnDel = generarBtnDel(titulo); // Genero el boton para borrar el campo
      fila.appendChild(btnDel); // Agrego el boton a la fila.
      const celdaLbl = document.createElement("td"); // Creo la celda que contendra el titulo de los radio buttons.
      celdaLbl.innerHTML = `${formatearTitulo(titulo)}:`;
      fila.appendChild(celdaLbl); // Agrego la celda a la fila.
      const celdaRadios = document.createElement("td"); // Creo la celda que contendra los radio buttons.
      inputs.forEach((element) => {
        // Recorro el arreglo de inputs.
        let valor = element.value; // Obtengo el titulo de un radio button.
        let unLabel = document.createElement("label"); // Creo el label para el titulo.
        let id = `idRadio${valor}${++idRadioBtn}`; // Creo el id y lo seteo.
        unLabel.setAttribute("for", `${id}`);
        unLabel.innerHTML = formatearTitulo(valor);
        let unRadio = document.createElement("input"); // Creo el radio button y seteo sus propiedades.
        unRadio.setAttribute("type", tipo);
        if (tipo === "radio") {
          unRadio.setAttribute("name", "radioBtn");
        } else if (tipo === "checkbox") {
          unRadio.setAttribute("name", `checkbox${idRadioBtn}`);
        }
        unRadio.setAttribute("id", `${id}`);
        celdaRadios.appendChild(unLabel); // Agrega el label a la celda.
        celdaRadios.appendChild(unRadio); // Agrega el radio button a la celda.
      });
      fila.appendChild(celdaRadios); // Agrega la celda que contiene todos los radio buttons generados.
      padre.appendChild(fila);
      contenido.push(titulo.toUpperCase()); // Agrega el titulo del campo al arreglo.
      resetearValores();
      borrarCamposOpciones();
    } else {
      alert(`Complete todos los campos.`);
    }
  } else {
    alert(`Seleccione una cantidad de radio buttons.`);
  }
}

function borrarCamposOpciones() {
  // Borra los campos cantidad y valores del formulario original.
  const padreSelecciones = document.getElementById("idSeleccionesForm");
  const cantRadio = document.getElementById("idCantidadRadio");
  const cant = document.getElementById("idFilaCantidad");
  const tipoBoton = document.getElementById("idTipoBoton");
  if (cantRadio) {
    padreSelecciones.removeChild(cantRadio);
  }
  if (cant) {
    padreSelecciones.removeChild(cant);
  }
  if (tipoBoton) {
    padreSelecciones.removeChild(tipoBoton);
  }
}

function generarOpcionesBoton() {
  // Agregar opciones para generar un boton.
  // Opciones: Tipo de boton, valor/contenido = Titulo.
  const padre = document.getElementById("idSeleccionesForm");
  //Creo fila
  const fila = document.createElement("tr");
  fila.setAttribute("id", "idTipoBoton");
  // Creo celda
  const celdaLabel = document.createElement("td");
  fila.appendChild(celdaLabel);
  // Creo label y lo agrego a la fila
  const label = document.createElement("label");
  label.setAttribute("for", "idOpcionesBoton");
  label.innerHTML = `Tipo:`;
  celdaLabel.appendChild(label);
  // Creo celda para opciones.
  const celdaOption = document.createElement("td");
  celdaOption.setAttribute("id", "idOpcionesBoton");
  fila.appendChild(celdaOption);
  // Creo selector y seteo propiedades
  const selector = document.createElement("select");
  selector.setAttribute("name", "tiposBoton");
  selector.setAttribute("id", "idTiposBoton");
  celdaOption.appendChild(selector);
  // Creo opcion submit
  const submit = document.createElement("option");
  submit.setAttribute("value", "submit");
  submit.text = `Submit`;
  selector.appendChild(submit);
  // Creo opcion reset
  const borrar = document.createElement("option");
  borrar.setAttribute("value", "reset");
  borrar.text = `Reset`;
  selector.appendChild(borrar);
  // Agrego fila al formulario.
  padre.appendChild(fila);
}

function agregarBoton(titulo) {
  //idTipoBoton = fila / idTiposBoton = selector
  const padre = document.getElementById("idContenidoForm");
  const opciones = document.getElementById("idTiposBoton");
  let index = opciones.selectedIndex;
  let tipo = opciones[index].value;
  if (botonesGenerados.includes(tipo)) {
    alert(`Usted ya genero un boton del tipo ${tipo}`);
  } else {
    let fila = document.getElementById("idFilaBotones");
    const celda = document.createElement("cd");
    celda.appendChild(generarBoton(titulo, tipo));
    if (fila) {
      fila.appendChild(celda);
    } else {
      fila = document.createElement("idFilaBotones");
      fila.setAttribute("id", "idFilaBotones");
      fila.appendChild(celda);
    }
    padre.appendChild(fila);
  }
}

function generarBoton(titulo, tipo) {
  const btn = document.createElement("button");
  let tituloF = titulo.replace(" ", "_");
  btn.setAttribute("id", `idBtn${titulo}`);
  btn.setAttribute("type", tipo);
  btn.setAttribute("value", tituloF);
  btn.innerHTML = formatearTitulo(titulo);
  botonesGenerados.push(tipo);
  return btn;
}
