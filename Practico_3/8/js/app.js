// Validar que ingrese una obra (2 inputs)
// Validar que ingrese un tipo (3 checkbox - Al menos 1 tiene que estar seleccionado)
// Valirdar que ingrese una disponibilidad (2 radio btns)
// Validar que ingrese un estilo (select)
const btnEnviar = document.getElementById("idBuscar");
btnEnviar.addEventListener("click", () => {
  const incompletos = [];
  let mensaje = ``;
  if (validarObra()) {
    incompletos.push(`Obra`);
  }
  if (validarTipo()) {
    incompletos.push(`Tipo`);
  }
  if (validarDisponibilidad()) {
    incompletos.push(`Disponibilidad`);
  }
  if (validarEstilo()) {
    incompletos.push(`Estilo`);
  }
  if (incompletos.length > 0) {
    let campos = incompletos.join("-");
    mensaje = `Llene los campos: ${campos}`;
    alert(mensaje);
  }
  incompletos.length = 0;
});

function validarObra() {
  const titulo = document.getElementById("idTitulo");
  const autor = document.getElementById("idAutor");
  return titulo.value === "" || autor.value === "";
}

function validarTipo () {
  const escultura = document.getElementById("idEscultura");
  const arquitectura = document.getElementById("idArquitectura");
  const pintura = document.getElementById("idPintura");
  return !((escultura.checked == 1) || (arquitectura.checked == 1) || (pintura.checked == 1));
}

function validarDisponibilidad() {
  const coleccion = document.getElementById("idColeccion");
  const museo = document.getElementById("idMuseo");
  return !(coleccion.checked || museo.checked);
}

function validarEstilo () {
  const estilo = document.getElementById("idEstilo");
  let i = estilo.selectedIndex;
  return i === 0;
}