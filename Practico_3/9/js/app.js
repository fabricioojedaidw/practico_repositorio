/**
 * Validar email.
 * Comentario maximo 100 caracteres. Informar cuantos caracteres le quedan disponibles.
 * Clave: min 6 caracteres / 1 minuscula / 1 mayuscula / 1 digito.
 * Preferencia: Al menos 1 debe estar seleccionada.
 */

const btnEnviar = document.getElementById("idEnviar");
btnEnviar.addEventListener("click", () => {
  const form = document.getElementById("form1");
  form.submit();
});

function contrasenaValida() {
  // Clave: min 6 caracteres / 1 minuscula / 1 mayuscula / 1 numero.
  const pw = String(document.getElementById("idContrasena1").value);
  if (pw.length < 6) {
    return false;
  } else {
    return contieneMayuscula(pw) && contieneMinuscula(pw) && contieneNumero(pw);
  }
}

function contieneMayuscula(cad) {
  let contiene = false;
  let i = 0;
  let c = "";
  do {
    c = String(cad[i]);
    if (isNaN(c)) {
      if (c === c.toUpperCase()) {
        contiene = true;
      }
    }
    i++;
    console.log(i, c);
  } while (!(contiene || i === cad.length));
  return contiene;
}

function contieneMinuscula(cad) {
  let contiene = false;
  let i = 0;
  let c = "";
  do {
    c = String(cad[i]);
    if (isNaN(c)) {
      if (c === c.toLowerCase()) {
        contiene = true;
      }
    }
    i++;
  } while (!(contiene || i === cad.length));
  return contiene;
}

function contieneNumero(cad) {
  let contiene = false;
  let i = 0;
  let c = "";
  do {
    c = cad[i];
    if (!isNaN(c)) {
      contiene = true;
    }
    i++;
  } while (!(contiene || i === cad.length));
  return contiene;
}
