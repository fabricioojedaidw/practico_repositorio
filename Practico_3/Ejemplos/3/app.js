window.onload = () => {
    imprimirCuerpo();
    invocoFunciones();
    analizoNumero();

};

function imprimirCuerpo() {
  document.write("<header>");
  document.write("<h1>Primer script con JS</h1>");
  document.write("</header>");
  document.write("<section>");
  document.write("<article>");
  document.write("<h2>Título h2 dentro de un artículo</h2>");
  document.write("</article>");
  document.write("</section>");
}

function invocoFunciones () {
    var a, b, c , resu = 0;
    var d = "25sd", e = "0.25asd";
    a = numeroAleatorio(20, 5);
    b = numeroAleatorio(10, 1);
    c = Math.round(Math.random() * 5);
    document.write("<section>");
    document.write("<article>");
    document.write("<h3>C&aacute;lculos</h3>");
    var resultados = `Valor de a: ${a} <br/>`;
    resultados += `Valor de b: ${b} <br/>`;
    resultados += `Valor de c: ${c}<br/>`;
    resu = eval("a + b");
    resultados += `Suma de a + b: ${resu}<br/>`;
    eval("c++");
    resultados += `Valor de c incrementado en 1: ${c}<br/>`;
    resultados += `Valor entero de la variable d: ${parseInt(d)}<br/>`;
    resultados += `Valor real de la variable e: ${parseFloat(e)}<br/>`;
    document.write(`<p>${resultados}</p>`);
    document.write("</article>");
    document.write("</section>");
  
}

function numeroAleatorio (a = 10, b = 0) {
    /**
     * a = maximo
     * b = minimo
     */
    return Math.round(Math.random() * (a - b) + b);
}

function analizoNumero () {
    let valor, mensaje;
    valor = prompt("Ingrese un valor numerico:");
    mensaje = (isNaN(valor) || valor === null) ? "El valor ingresado no es numerico." : "El valor ingresado es numerico.";
    alert(mensaje);
    console.log(valor);
}