const btnMultiplicar = document.getElementById("btnMultiplicar");
const btnSumar = document.getElementById("btnSumar");
const result = document.getElementById("idResult");
const num1 = document.getElementById("idNum1");
const num2 = document.getElementById("idNum2");

// Ejercicio a
btnMultiplicar.addEventListener("click", () => {
  calcular("*");
});
btnSumar.addEventListener("click", () => {
  calcular("+");
});
function calcular(op) {
  if (num1.value !== "" && num2.value !== "") {
    let resultado = eval("parseInt(num1.value)" + op + "parseInt(num2.value)");
    result.innerHTML = `Resultado: ${resultado}`;
  }
}

// Ejercicio b
const btnCalcular = document.getElementById("btnCalcular");
const num3 = document.getElementById("idNum3");
const num4 = document.getElementById("idNum4");
const result2 = document.getElementById("idResult2");

btnCalcular.addEventListener("click", () => {
  let resultado;
  if (num3.value !== "" && num4.value !== "") {
    if (num3.value > num4.value) {
      resultado = calcularPotencia();
    } else if (num3.value <= num4.value) {
      resultado = calcularDivision();
    }
  }
  result2.innerHTML = `Resultado: ${resultado}`;
});

function calcularPotencia() {
  return `Potencia de ${num3.value}^${num4.value} = ${Math.pow(parseInt(num3.value), parseInt(num4.value))}`;
}

function calcularDivision () {
  if (parseInt(num4.value) !== 0) {
    return `Divisi&oacute;n de ${num3.value}/${num4.value} = ${parseInt(num3.value)/parseInt(num4.value)}`;
  } else {
    return `No es posible dividir por 0.`;
  }
}

function restasSucesivas (a, b) {
  let resultado = 0;
  while () {}
}

//Ejercicio c
const btnFecha = document.getElementById("btnFecha");
btnFecha.addEventListener("click", () => {
  const mostrarFecha = document.getElementById("idFecha");
  let fecha = new Date();
  mostrarFecha.innerHTML = `${fecha}`;
});

function obtenerFecha () {
  let fecha = new Date();
  let dia = fecha.getDay();
  let mes = fecha.getMonth();
  let anio = fecha.getFullYear();
  let resultado = ``;
}