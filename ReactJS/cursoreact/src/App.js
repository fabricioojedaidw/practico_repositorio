import logo from './logo.svg';
import './App.css';
import { useState } from 'react';
import html2canvas from 'html2canvas';

function App() {
  // Estados de React - linea1 = variable, setLinea1 = funcion para setear el estado
  
  const [linea1, setLinea1] = useState('');
  const [linea2, setLinea2] = useState('');
  const [imagen, setImagen] = useState('fire');

  const cargarValor = (event) => {
    let id = event.target.id;
    if (id === "idLinea1") {
      setLinea1(event.target.value);
    } else  if (id === "idLinea2"){
      setLinea2(event.target.value);
    }
  };
  const cargarImagen = (event) => {
    setImagen(event.target.value);
  };
  const exportarImagen = () => {
    html2canvas(document.querySelector("#meme")).then(canvas => {
      let img = canvas.toDataURL("image/png");
      let link = document.createElement("a");
      link.download = "meme.png";
      link.href = img;
      link.click();
  });
  };

  return (
    <div className="App">
      {/* Select picker de memes */}
      <select onChange={cargarImagen}>
        <option value="fire">Casa en llamas</option>
        <option value="futurama">Futurama</option>
        <option value="history">History Channel</option>
        <option value="matrix">Matrix</option>
        <option value="philosoraptor">Philosoraptor</option>
        <option value="smart">Smart Guy</option>
      </select> <br/>
      {/* Input text - Primer linea */}
      <input id="idLinea1" onChange={cargarValor} type="text" placeholder='Linea 1'/> <br/>
      {/* Input text - Segunda linea */}
      <input id="idLinea2" onChange={cargarValor} type="text" placeholder='Linea 2'/><br/>
      {/* Boton para exportar */}
      <button onClick={exportarImagen}>Exportar</button>
      <div className='meme' id='meme'>
        <span>{linea1}</span> <br/>
        <span>{linea2}</span>
        <img src={`/memes/${imagen}.jpg`} alt={`${imagen}.jpg`}/>
      </div>
    </div>
  );
}

export default App;
