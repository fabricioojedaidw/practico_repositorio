var cantClicks = 0;
var turno = '';
var ganador = false;
var mensaje = '';
const tateti = [];

const crearTablero = (columns, rows) => {
    let tabla = `<table>`;
    for (let i = 0; i < columns; i++) {
        let fila = `<tr>`;
        const row = [];
        for (let j = 0; j < rows; j++) {
            fila += `<td><input type='button' id='idBtn${i}${j}' class='celda' value=' ' ></input></td>`;
            row.push(j);
        }
        tabla += `${fila}</tr>`;
        tateti.push(row);
    }
    return `${tabla}</table>`;
}

const pasarTurno = (turno) => {
    return turno === 'X' ? 'O' : 'X';
};

const horizontal = () => {
    let iguales = false;
    let i = 0;
    while (!iguales && (i < tateti.length)) {
        iguales = tateti[i].every(i => i == 'O') || tateti[i].every(i => i == 'X');
        i++;
    }
    return iguales;
};

const vertical = () => {
    const col = [];
    for (let i = 0; i < tateti.length; i++) {
        for (let j = 0; j < tateti.length; j++) {
            col.push(tateti[j][i]);
        }
        if (col.every(i => i == 'O') || col.every(i => i == 'X')) {
            return true;
        }
        col.length = 0;
    }
    return false;
};

const traza = () => {
    const t1 = [];
    const t2 = [];
    for (let i = 0; i < tateti.length; i++) {
        t1.push(tateti[i][i]);
        t2.push(tateti[i][tateti.length - 1 - i]);
    }
    return (t1.every(i => i == 'O') || t1.every(i => i == 'X')) || (t2.every(i => i == 'O') || t2.every(i => i == 'X'));
};

const jugarTurno = (e) => {
    cantClicks++;
    e.target.value = turno;
    e.target.disabled = true;
    const i = e.target.id[e.target.id.length - 2];
    const j = e.target.id[e.target.id.length - 1];
    tateti[i][j] = turno;
    if (cantClicks >= 5) {
        if (vertical() || traza() || horizontal()) {
            ganador = true;
            mensaje = `Ganador ${turno}!!! Felicitaciones!!!`;
        }
    }
    if (ganador) {
        document.getElementById('idPGanador').innerText = mensaje;
        const inputs = Array.from(document.getElementsByClassName('celda'));
        inputs.forEach(input => {
            input.disabled = true;
        });
    } else {
        turno = pasarTurno(turno);
        document.getElementById('idPTurno').innerText = `Turno del jugador: ${turno}`;
    }
    if (cantClicks === 9 && !ganador) {
        document.getElementById('idPGanador').innerText = `Empate!!!`;
    }
};

const iniciarJuego = () => {
    ganador = false;
    mensaje = '';
    cantClicks = 0;
    tateti.length = 0;
    document.getElementById('idPGanador').innerText = ``;
    document.getElementById('idDivTablero').innerHTML = crearTablero(3, 3);
    turno = Math.floor(Math.random() * (2 - 1 + 1) + 1) === 1 ? 'X' : 'O';
    document.getElementById('idPTurno').innerText = `Turno del jugador: ${turno}`;
    const inputs = Array.from(document.getElementsByClassName('celda'));
    inputs.forEach(input => {
        input.addEventListener('click', (e) => {
            jugarTurno(e);
        });
    });
};

document.getElementById('idBtnIniciarJuego').addEventListener('click', iniciarJuego);

iniciarJuego();